#ifndef _COMMON_H
#define _COMMON_H

#include <rtg.h>

typedef struct tile_s {
	int base[2];
	int overlay[2];
	int click;
	int shade;
} tile_t;

typedef struct village_unit_s {
	struct village_unit_s *prev;
	struct village_unit_s *next;
	int id;
	int owner;
	int type;
	int hp;
	int action;
	int reset;
	int pos[2];
	int target[2];
	array_t *path;
	anim_instance_t *anim;
} village_unit_t;

typedef struct villagedata_s {
	int x;
	int y;
	int state;
	tile_t **tiles;
	animation_t **ground;
	animation_t **unit;
	float centre_x;
	float centre_y;
	int draw_width;
	int draw_height;
	int home_x;
	int home_y;
	village_unit_t *units;
} villagedata_t;

typedef struct drawspec_s {
	color_t c;
	int texture;
	int sprite;
	int click;
} drawspec_t;

typedef struct mapnode_s {
	unsigned char shade;
	unsigned char height;
	unsigned char detail;
	unsigned char tile;
} __attribute__((packed)) mapnode_t;

typedef struct unit_s {
	struct unit_s *prev;
	struct unit_s *next;
	int id;
	int owner;
	int type;
	int hp;
	int pos;
	int target;
	int step;
	int focus;
	int action;
	array_t *path;
} unit_t;

#define MAP_BLANK	0x00
#define MAP_GRASS	0x01
#define MAP_SAND	0x02
#define MAP_WATER	0x03
#define MAP_PATH	0x04
#define MAP_VC1		0x05
#define MAP_VC2		0x06
#define MAP_OUTPOST	0x07
#define MAP_WALL1	0x08
#define MAP_WALL2	0x09
#define MAP_FARM1	0x0A
#define MAP_HUT		0x0B
#define MAP_BARRACKS1	0x0C
#define MAP_BARRACKS2	0x0D
#define MAP_GRASSTREE1	0x0E
#define MAP_GRASSTREE2	0x0F
#define MAP_GRASSTREE3	0x10
#define MAP_GRASSTREE4	0x11
#define MAP_SANDTREE1	0x12
#define MAP_SANDTREE2	0x13
#define MAP_SANDTREE3	0x14
#define MAP_SANDTREE4	0x15
#define MAP_SHRUB1	0x16
#define MAP_SHRUB2	0x17
#define MAP_SHRUB3	0x18
#define MAP_SHRUB4	0x19

#define MAP_ALLY	0x40
#define MAP_OWN		0x80

#define ACTION_ATTACK	1
#define ACTION_TRAIN	2
#define ACTION_UPGRADE	3
#define ACTION_QUIT	4

#define ACTERR_SYSTEM	1
#define ACTERR_UPGRADE	2
#define ACTERR_NOUNITS	3
#define ACTERR_NOCREDIT	4
#define ACTERR_NOTARGET	5
#define ACTERR_DEFEATED	6

#define UNIT_VILLAGER	0x00
#define UNIT_FARMER	0x01
#define UNIT_MASTER	0x02
#define UNIT_SOLDIER	0x03
#define UNIT_BRIGADIER	0x04
#define UNIT_SCOUT	0x05
#define UNIT_COUNT	0x06

#define CLIENT_HUMAN	0
#define CLIENT_AI	1

#define PLAYER_DEFEATED	1
#define PLAYER_WON	2
#define PLAYER_END	3

#define UACT_STAND	0x00
#define UACT_MOVE	0x10
#define UACT_TEND	0x20
#define UACT_ATTACK	0x30

#define SHADE_HIDDEN	0x00
#define SHADE_DARK 	0x01
#define SHADE_BRIGHT	0x02

typedef struct unit_info_s {
	struct unit_info_s *prev;
	struct unit_info_s *next;
	int id;
	int owner;
	int type;
	int hp;
	int action;
	array_t *path;
} unit_info_t;

typedef struct client_s {
	int mode;
	mutex_t *mut;
	mapnode_t *cmap;
	unit_info_t *cunits;
	int cmaps[2];
	int cid;
} client_t;

typedef struct comset_s {
	int type;
	void (*unit)(int id, int owner, int unit, int uid, int hp, int action, array_t *path);
	void (*msg)(int id, char* t);
	void (*sig)(int id, char* t);
	void (*data)(int id, int c, int s, int u[UNIT_COUNT]);
	void (*map)(int id, mapnode_t *data, int w, int h);
	void (*state)(int id, int state);
	void (*init)(int id, int vg_c, int fm_c, int mf_c, int sd_c, int bg_c, int sc_c, int ug_c, array_t *players);
} comset_t;

typedef struct playerdata_s {
	int id;
	int credits;
	int strength;
	int upgrading;
	int defeated;
	int units[UNIT_COUNT];
	int t_units[UNIT_COUNT];
	int ready;
	int has_trees;
	int has_paths;
	int has_outposts;
	int has_walls;
	unsigned char *shade;
} playerdata_t;

typedef struct ai_s {
	int id;
	int credits;
	int strength;
	int upgrading;
	int defeated;
	int units[UNIT_COUNT];
	int last;
} ai_t;

typedef struct action_s {
	int action;
	int owner;
	int target;
	int doin;
	int units[UNIT_COUNT];
} action_t;

typedef struct gamedata_s {
	array_t *actions;
	thread_t *thread;
	array_t *players;
	int state;
	int turn_time;
	int difficulty;
	int turns;
} gamedata_t;

typedef struct mapgen_s {
	int w;
	int h;
	int l;
	mapnode_t* data;
	int pc;
	struct mapplayer *players;
} mapgen_t;

/* defined in lib.c */
int rand_range(int low, int high);
int direction(int bx, int by, int ex, int ey);
int distance(int bx, int by, int ex, int ey);

/* defined in village.c */
extern villagedata_t village;
void village_move_up(void);
void village_move_down(void);
void village_move_left(void);
void village_move_right(void);
void village_select(void);
void village_unload(void);
void village_set(int x, int y, int bt, int b, int ot, int o, int c, int s);
void village_load(int x, int y);
void village_draw(void);
int village_to_screen(int x, int y, int *sx, int *sy);
int village_from_screen(int sx, int sy, int *x, int *y);
tile_t *village_get_tile(int x, int y);
void village_goto(int x, int y);
void village_set_home(int x, int y);
int village_get_home(void);
void village_gohome(void);
village_unit_t *village_unit(int id);
village_unit_t *village_unit_at(int x, int y);

/* defined in map.c */
void map_select(void);
void map_toggle(void);
int map_toggle_ui(widget_t *w);
void map_move_up(void);
void map_move_down(void);
void map_move_left(void);
void map_move_right(void);
void map_load(mapnode_t *data, int w, int h);
void map_unload(void);
int map_from_screen(int sx, int sy, int *x, int *y);
mapnode_t *map_get_node(int x, int y);

/* defined in drawspec.c */
drawspec_t *drawspec_get(int i);

/* defined in mapgen.c */
extern mapgen_t mapgen;
mapnode_t *mapgen_get(int x, int y);
int mapgen_get_detail(int x, int y);
int mapgen_get_tile(int x, int y);
int mapgen_init(int players);
void mapgen_free(void);
void mapgen_send(void);
int mapgen_find(int player, int type);

/* defined in noise.c */
extern int seed;
void noise_seed(int s);
double noise2d(int x, int y);
unsigned char noise(int x, int y);
unsigned char noise_height(int x, int y);

/* defined in game.c */
extern gamedata_t game;
int game_init(void);
void game_exit(void);
int game_push_action(action_t *a);
int game_get_cost(playerdata_t *p, int type, int count);
playerdata_t *game_get_playerdata(int id);

/* defined in player.c */
extern char* acterrors[];
int player_connect(char* name, comset_t *c);
void player_disconnect(int id);
void player_unit(int id, int owner, int unit, int uid, int hp, int action, array_t *path);
void player_message(int id, char* str, ...);
void player_message_all(char* str, ...);
void player_signal(int id, char* str, ...);
void player_update(int id, int c, int s, int u[UNIT_COUNT]);
void player_map(int id, mapnode_t *data, int w, int h);
void player_state(int id, int state);
void player_init(int id, int vg_c, int fm_c, int mf_c, int sd_c, int bg_c, int sc_c, int ug_c, array_t *players);
char* player_name(int id);
int player_is_ai(int id);
int player_is_defeated(int id);
int player_count(void);

/* defined in client.c */
extern client_t client;
void client_connect_local(void);
void client_connect_remote(char* server, char* port);
void client_disconnect(void);
int client_is_local(void);
void client_main(void);
int client_train_villager(widget_t *w);
int client_train_farmer(widget_t *w);
int client_train_master(widget_t *w);
int client_train_soldier(widget_t *w);
int client_train_brigadier(widget_t *w);
int client_train_scout(widget_t *w);
int client_attack(widget_t *w);

/* defined in texteffect.c */
void texteffect_add(char* str);
void texteffect_draw(void);
void texteffect_clear(void);

/* defined in unit.c */
void unit_spawn(playerdata_t *p, int type, int count);
void unit_step(void);
unit_t *unit_find_villager(int id);
unit_t *unit_find_pos(int pos);
unit_t *unit_find_target(int pos, int except);

/* defined in ai.c */
int ai_init(int count);
void ai_exit(void);
int ai_doturns(int turn);

/* defined in scout.c */
void scout_notify(playerdata_t *p, playerdata_t *e, int event);

/* defined in path.c */
array_t *path_find(int x, int y, int ex, int ey);
void path_free(array_t *a);
array_t *path_copy(array_t *o);

/* defined in shade.c */
void shade_create(int x, int y);
void shade_reset(void);
void shade_set(int id, int x, int y, unsigned char v);
unsigned char shade_get(int id, int x, int y);

/* defined in gui.c */
void gui_init(void);

#endif
