/************************************************************************
* game.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

gamedata_t game = {
	NULL,
	NULL,
	NULL,
	0,
	60,
	1,
	0
};

playerdata_t *game_get_playerdata(int id)
{
	playerdata_t **p = game.players->data;
	id--;
	if (id < 0 || id >= game.players->length)
		return NULL;

	return p[id];
}

static int game_tpt(playerdata_t *p, int type)
{
	int t = 0;
	switch (type) {
	case UNIT_VILLAGER:
		t = p->units[UNIT_VILLAGER]/8;
		if (t < 1)
			t = 1;
		break;
	case UNIT_FARMER:
		t = p->units[UNIT_VILLAGER]/8;
		t += p->units[UNIT_FARMER];
		t += p->units[UNIT_MASTER];
		if (t < 1)
			t = 1;
		break;
	case UNIT_MASTER:
		t = p->units[UNIT_VILLAGER]/8;
		t += p->units[UNIT_FARMER];
		t += p->units[UNIT_MASTER];
		t = t/5;
		break;
	case UNIT_SOLDIER:
		t = p->units[UNIT_VILLAGER]/10;
		t += p->units[UNIT_SOLDIER]/5;
		t += p->units[UNIT_BRIGADIER];
		if (t < 1)
			t = 1;
		break;
	case UNIT_BRIGADIER:
		t = p->units[UNIT_VILLAGER]/10;
		t += p->units[UNIT_SOLDIER]/5;
		t += p->units[UNIT_BRIGADIER];
		t = t/5;
		break;
	case UNIT_SCOUT:
		t = p->units[UNIT_VILLAGER]/8;
		t += p->units[UNIT_FARMER]/4;
		t += p->units[UNIT_MASTER];
		t += p->units[UNIT_SOLDIER]/5;
		t += p->units[UNIT_BRIGADIER];
		t = t/5;
		break;
	default:;
	}
	t -= p->t_units[type];
	if (t < 0)
		return 0;
	return t;
}

static int game_train(playerdata_t *p, int type, int count)
{
	int t = game_tpt(p,type);
	int c;
	if (!t)
		return count;
	if (t > count)
		t = count;
	c = game_get_cost(p,type,t);

	switch (type) {
	case UNIT_VILLAGER:
		rtprintf(CN_DEBUG "game_train: UNIT_VILLAGER");
		p->credits -= c;
		p->units[UNIT_VILLAGER] += t;
		p->t_units[UNIT_VILLAGER] += t;
		player_message(p->id,"You have trained a new Villager");
		break;
	case UNIT_FARMER:
		p->credits -= c;
		p->units[UNIT_FARMER] += t;
		p->t_units[UNIT_FARMER] += t;
		p->units[UNIT_VILLAGER] -= t;
		player_message(p->id,"You have trained a new Farmer");
		break;
	case UNIT_MASTER:
		p->credits -= c;
		p->units[UNIT_MASTER] += t;
		p->t_units[UNIT_MASTER] += t;
		p->units[UNIT_FARMER] -= t;
		player_message(p->id,"You have trained a new Master Farmer");
		break;
	case UNIT_SOLDIER:
		p->credits -= c;
		p->units[UNIT_SOLDIER] += t;
		p->t_units[UNIT_SOLDIER] += t;
		p->units[UNIT_VILLAGER] -= t;
		player_message(p->id,"You have trained a new Soldier");
		break;
	case UNIT_BRIGADIER:
		p->credits -= c;
		p->units[UNIT_BRIGADIER] += t;
		p->t_units[UNIT_BRIGADIER] += t;
		p->units[UNIT_SOLDIER] -= t;
		player_message(p->id,"You have trained a new Brigadier");
		break;
	case UNIT_SCOUT:
		p->credits -= c;
		p->units[UNIT_SCOUT] += t;
		p->t_units[UNIT_SCOUT] += t;
		p->units[UNIT_VILLAGER] -= t;
		player_message(p->id,"You have trained a new Scout");
		break;
	default:;
	}
	unit_spawn(p,type,t);
	return count-t;
}

static int game_attack(action_t *a)
{
	/* TODO: graphicalise it */
	int c;
	int w;
	int l;
	int au;
	int du;
	int i;
	playerdata_t *p;
	playerdata_t *e;

	p = game_get_playerdata(a->owner);
	e = game_get_playerdata(a->target);
	/* if either player has already been defeated then there's nothing to fight for */
	if (!p || p->defeated)
		return 0;
	/* return the army */
	if (!e || e->defeated)
		return ACTERR_NOTARGET;

	/* get unit counts (1 brig = 5 soldier) */
	au = (a->units[UNIT_SOLDIER] + (a->units[UNIT_BRIGADIER]*5));
	du = (e->units[UNIT_SOLDIER]+(e->units[UNIT_BRIGADIER]*5));
	/* unit count * strength = true units */
	au = (au*p->strength);
	du = (du*e->strength);
	/* scouts mean better knowledge of the enemy, and a better chance at winning */
	if (p->units[UNIT_SCOUT] > 0) {
		au = floor((double)au*(1.2*(double)p->units[UNIT_SCOUT]));
	}
	if (e->units[UNIT_SCOUT] > 0) {
		du = floor((double)du*(1.2*(double)e->units[UNIT_SCOUT]));
	}

	/* deduct smaller number from higher number */
	if (au < du) {
		c = (du-au)+1;
		/* c > 0 means attack troops have a 1 in c chance */
		/* rand_range the chance */
		w = rand_range(0,c);
		/* determine defeated team */
		/* defender is defeated means total loss */
		if (w == 0) {
			player_message_all("'%s' defeated '%s'!",player_name(p->id),player_name(e->id));
			e->defeated = 1;
			player_state(e->id,PLAYER_DEFEATED);
			/* whatever the chance was, that % of units gets added to winner
			 * as well as their own units back, and all of the losers credits */
			p->units[UNIT_VILLAGER] += (int)ceil(e->units[UNIT_VILLAGER]*(c/100));
			p->units[UNIT_FARMER] += (int)ceil(e->units[UNIT_FARMER]*(c/100));
			p->units[UNIT_SOLDIER] += (int)ceil(e->units[UNIT_SOLDIER]*(c/100))+a->units[UNIT_SOLDIER];
			p->units[UNIT_BRIGADIER] += a->units[UNIT_BRIGADIER];
			p->credits += e->credits;

			e->credits = 0;
			e->strength = 0;
			e->upgrading = 0;
			for (i=0; i<UNIT_COUNT; i++) {
				e->units[i] = 0;
			}
		}else{
			if (a->units[UNIT_SOLDIER] < e->units[UNIT_SOLDIER])
				l = (int)floor(e->units[UNIT_SOLDIER] - a->units[UNIT_SOLDIER]);
			else
				l = (int)floor(a->units[UNIT_SOLDIER] - e->units[UNIT_SOLDIER]);
			e->units[UNIT_SOLDIER] -= l;
			if (e->units[UNIT_SOLDIER] < 1)
				e->units[UNIT_SOLDIER] = 0;
			player_message(p->id,"'%s' successfully defended against your attack!",player_name(e->id));
			player_message(e->id,"You succesfully defended against '%s's attack!",player_name(p->id));
		}
	}else{
		c = (au-du)+1;
		/* c > 0 means defence troops have a 1 in c chance */
		/* rand_range the chance */
		w = rand_range(0,c);
		/* determine defeated team */
		/* defender is defeated means total loss */
		if (w > 0) {
			player_message_all("'%s' defeated '%s'!",player_name(p->id),player_name(e->id));
			e->defeated = 1;
			player_state(e->id,PLAYER_DEFEATED);
			/* whatever the chance was, that % of units gets added to winner
			 * as well as their own units back, and all of the losers credits */
			p->units[UNIT_VILLAGER] += (int)ceil(e->units[UNIT_VILLAGER]*(c/100));
			p->units[UNIT_FARMER] += (int)ceil(e->units[UNIT_FARMER]*(c/100));
			p->units[UNIT_SOLDIER] += (int)ceil(e->units[UNIT_SOLDIER]*(c/100))+a->units[UNIT_SOLDIER];
			p->units[UNIT_BRIGADIER] += a->units[UNIT_BRIGADIER];
			p->credits += e->credits;

			e->credits = 0;
			e->strength = 0;
			e->upgrading = 0;
			for (i=0; i<UNIT_COUNT; i++) {
				e->units[i] = 0;
			}
		}else{
			if (a->units[UNIT_SOLDIER] < e->units[UNIT_SOLDIER])
				l = (int)floor(e->units[UNIT_SOLDIER] - a->units[UNIT_SOLDIER]);
			else
				l = (int)floor(a->units[UNIT_SOLDIER] - e->units[UNIT_SOLDIER]);
			e->units[UNIT_SOLDIER] -= l;
			if (e->units[UNIT_SOLDIER] < 1)
				e->units[UNIT_SOLDIER] = 0;
			player_message(p->id,"'%s' successfully defended against your attack!",player_name(e->id));
			player_message(e->id,"You succesfully defended against '%s's attack!",player_name(p->id));
		}
	}
	return 0;
}

static int game_do_action(action_t *a)
{
	playerdata_t *p;
	playerdata_t *e;
	int c;
	int cost[7];
	if (!a)
		return 1;

	p = game_get_playerdata(a->owner);
	e = game_get_playerdata(a->target);
	if (!p)
		return 1;
	if (p->upgrading)
		return ACTERR_UPGRADE;

	if (p->defeated) {
		free(a);
		return 0;
	}

	switch (a->action) {
	case ACTION_ATTACK:
		if (!e || e->defeated)
			return ACTERR_NOTARGET;
		if (p->units[UNIT_SOLDIER] < a->units[UNIT_SOLDIER])
			return ACTERR_NOUNITS;
		if (p->units[UNIT_BRIGADIER] < a->units[UNIT_BRIGADIER])
			return ACTERR_NOUNITS;
		return game_attack(a);
		break;
	case ACTION_TRAIN:
		if (a->units[UNIT_VILLAGER]) {
			if (game_get_cost(p,UNIT_VILLAGER,a->units[UNIT_VILLAGER]) > p->credits)
				return ACTERR_NOCREDIT;
			c = game_train(p,UNIT_VILLAGER,a->units[UNIT_VILLAGER]);
			if (c) {
				a->units[UNIT_VILLAGER] = c;
				return -1;
			}
		}
		if (a->units[UNIT_FARMER]) {
			if (p->units[UNIT_VILLAGER] < a->units[UNIT_FARMER])
				return ACTERR_NOUNITS;
			if (game_get_cost(p,UNIT_FARMER,a->units[UNIT_FARMER]) > p->credits)
				return ACTERR_NOCREDIT;
			c = game_train(p,UNIT_FARMER,a->units[UNIT_FARMER]);
			if (c) {
				a->units[UNIT_FARMER] = c;
				return -1;
			}
		}
		if (a->units[UNIT_MASTER]) {
			if (p->units[UNIT_VILLAGER] < a->units[UNIT_MASTER])
				return ACTERR_NOUNITS;
			if (game_get_cost(p,UNIT_MASTER,a->units[UNIT_MASTER]) > p->credits)
				return ACTERR_NOCREDIT;
			c = game_train(p,UNIT_MASTER,a->units[UNIT_MASTER]);
			if (c) {
				a->units[UNIT_MASTER] = c;
				return -1;
			}
		}
		if (a->units[UNIT_SOLDIER]) {
			if (p->units[UNIT_VILLAGER] < a->units[UNIT_SOLDIER])
				return ACTERR_NOUNITS;
			if (game_get_cost(p,UNIT_SOLDIER,a->units[UNIT_SOLDIER]) > p->credits)
				return ACTERR_NOCREDIT;
			c = game_train(p,UNIT_SOLDIER,a->units[UNIT_SOLDIER]);
			if (c) {
				a->units[UNIT_SOLDIER] = c;
				return -1;
			}
		}
		if (a->units[UNIT_BRIGADIER]) {
			if (p->units[UNIT_VILLAGER] < a->units[UNIT_BRIGADIER])
				return ACTERR_NOUNITS;
			if (game_get_cost(p,UNIT_BRIGADIER,a->units[UNIT_BRIGADIER]) > p->credits)
				return ACTERR_NOCREDIT;
			c = game_train(p,UNIT_BRIGADIER,a->units[UNIT_BRIGADIER]);
			if (c) {
				a->units[UNIT_BRIGADIER] = c;
				return -1;
			}
		}
		if (a->units[UNIT_SCOUT]) {
			if (p->units[UNIT_VILLAGER] < a->units[UNIT_SCOUT])
				return ACTERR_NOUNITS;
			if (game_get_cost(p,UNIT_SCOUT,a->units[UNIT_SCOUT]) > p->credits)
				return ACTERR_NOCREDIT;
			c = game_train(p,UNIT_SCOUT,a->units[UNIT_SCOUT]);
			if (c) {
				a->units[UNIT_SCOUT] = c;
				return -1;
			}
		}
		break;
	case ACTION_UPGRADE:
		c = game_get_cost(p,ACTION_UPGRADE,0);
		p->upgrading = 0;
		if (c > p->credits)
			return ACTERR_NOCREDIT;
		p->credits -= c;
		p->strength++;
		cost[0] = game_get_cost(NULL,UNIT_VILLAGER,1);
		cost[1] = game_get_cost(NULL,UNIT_FARMER,1);
		cost[2] = game_get_cost(NULL,UNIT_MASTER,1);
		cost[3] = game_get_cost(NULL,UNIT_SOLDIER,1);
		cost[4] = game_get_cost(NULL,UNIT_BRIGADIER,1);
		cost[5] = game_get_cost(NULL,UNIT_SCOUT,1);
		cost[6] = game_get_cost(p,ACTION_UPGRADE,0);
		player_init(p->id,cost[0],cost[1],cost[2],cost[3],cost[4],cost[5],cost[6],NULL);
		break;
	default:
		return 1;
	}

	free(a);

	return 0;
}

static void *game_main(thread_t *t)
{
	int d;
	int i;
	int r;
	int s;
	int w = -1;
	int cost[7];
	playerdata_t *p;
	playerdata_t *e;
	action_t *a;
	array_t *wa = array_create(RTG_TYPE_STRING);
	game.state = 1;

	game.turn_time = config_get_int("game.turn_time");
	if (game.turn_time < 10)
		game.turn_time = 10;
	game.turn_time *= 1000;

	game.difficulty = config_get_int("game.difficulty");
	if (game.difficulty < 1)
		game.difficulty = 1;

	w = config_get_int("game.ais");

	ai_init(w);

	d = player_count();

	if (d < 2) {
		player_message_all("Cannot play with only 1 player, try adding some AIs");
		thread_exit(t,0);
		return NULL;
	}

	cost[0] = game_get_cost(NULL,UNIT_VILLAGER,1);
	cost[1] = game_get_cost(NULL,UNIT_FARMER,1);
	cost[2] = game_get_cost(NULL,UNIT_MASTER,1);
	cost[3] = game_get_cost(NULL,UNIT_SOLDIER,1);
	cost[4] = game_get_cost(NULL,UNIT_BRIGADIER,1);
	cost[5] = game_get_cost(NULL,UNIT_SCOUT,1);
	cost[6] = 0;

	for (i=0; i<d; i++) {
		array_push_string(wa,player_name(i+1));
	}
	for (i=0; i<d; i++) {
		p = malloc(sizeof(playerdata_t));
		p->id = i+1;
		p->ready = 0;
		p->credits = 20;
		for (r=0; r<UNIT_COUNT; r++) {
			p->units[r] = 0;
			p->t_units[r] = 0;
		}
		p->units[UNIT_VILLAGER] = 2;
		p->strength = 1;
		p->upgrading = 0;
		p->defeated = 0;
		p->has_trees = 1;
		p->has_paths = 0;
		p->has_outposts = 0;
		p->has_walls = 0;
		p->shade = NULL;
		array_push_ptr(game.players,p);
		player_update(p->id,p->credits,p->strength,p->units);
		if (!cost[6])
			cost[6] = game_get_cost(p,ACTION_UPGRADE,0);
		player_init(p->id,cost[0],cost[1],cost[2],cost[3],cost[4],cost[5],cost[6],wa);
	}

	array_free(wa);

	player_message_all("Generating Map...");
	mapgen_init(d);
	shade_reset();
	for (i=0; i<d; i++) {
		p = array_get_ptr(game.players,i);
		unit_spawn(p,UNIT_VILLAGER,2);
	}
	mapgen_send();

	game.turns = 1;
	player_message_all("Turn 1 started with %d Players (%d Human, %d AI)",d,d-w,w);
	w = -1;
	wa = array_create(RTG_TYPE_PTR);

	while (game.state) {
		d = 0;
		while (d < game.turn_time) {
			delay(game.turn_time/10);
			if (!game.state)
				break;
			d += game.turn_time/10;
			r = 1;
			for (i=0; i<game.players->length; i++) {
				p = array_get_ptr(game.players,i);
				if (p && !p->ready) {
					r = 0;
					break;
				}
			}
			while (rtg_state == RTG_STATE_PAUSED) {
				delay(10);
			}
			if (r)
				break;
			ai_doturns(game.turns);
			if (!(d%1000) && (s = ((game.turn_time-d)/1000)) > 0) {
				if (s < 6 || !(s%10))
					player_message_all("Turn %d ends in %d Seconds",game.turns,s);
			}
		}
		shade_reset();
		unit_step();
		mapgen_send();
		if (!game.state)
			break;
		ai_doturns(-1);
		player_message_all("Turn %d Complete",game.turns);
		for (i=0; i<game.players->length; i++) {
			p = array_get_ptr(game.players,i);
			for (d=0; d<UNIT_COUNT; d++) {
				p->t_units[d] = 0;
			}
		}
		while ((a = array_pop_ptr(game.actions))) {
			a->doin--;
			if (a->doin) {
				if (a->action == ACTION_ATTACK && a->doin == 1) {
					p = game_get_playerdata(a->owner);
					e = game_get_playerdata(a->target);
					scout_notify(p,e,ACTION_ATTACK);
				}
				array_push_ptr(wa,a);
			}else{
				d = game_do_action(a);
				if (d < 0) {
					a->doin = 1;
					array_push_ptr(wa,a);
				}else if (d) {
					player_message(a->owner,acterrors[d]);
					free(a);
				}
			}
		}
		while ((a = array_pop_ptr(wa))) {
			array_push_ptr(game.actions,a);
		}
		d = 0;
		for (i=0; i<game.players->length; i++) {
			p = array_get_ptr(game.players,i);
			if (p->defeated)
				d++;
			player_update(p->id,p->credits,p->strength,p->units);
		}
		/* find the winner */
		if (d > game.players->length-2) {
			w = -1;
			for (i=0; i<game.players->length; i++) {
				p = array_get_ptr(game.players,i);
				if (!p->defeated) {
					w = p->id;
					break;
				}
			}
			game.state = 0;
			break;
		/* see if there's still a human playing */
		}else{
			w = 0;
			for (i=0; i<game.players->length; i++) {
				p = array_get_ptr(game.players,i);
				if (!p->defeated && !player_is_ai(p->id)) {
					w = 1;
					break;
				}
			}
			if (!w) {
				w = -1;
				game.state = 0;
				break;
			}
		}
		game.turns++;
		player_message_all("Turn %d Started",game.turns);
		while (rtg_state == RTG_STATE_PAUSED) {
			delay(10);
		}
	}

	if (w > -1) {
		player_message_all("Player '%s' won in %d turns!",player_name(w),game.turns);
	}else{
		player_message_all("You all died, lol");
	}
	for (i=0; i<game.players->length; i++) {
		p = array_get_ptr(game.players,i);
		if (p->id == w) {
			player_state(p->id,PLAYER_WON);
		}else{
			player_state(p->id,PLAYER_END);
		}
	}

	while ((a = array_pop_ptr(game.actions))) {
		free(a);
	}

	while ((p = array_pop_ptr(game.players))) {
		free(p);
	}

	ai_exit();
	mapgen_free();

	array_free(wa);

	thread_exit(t,0);
	return NULL;
}

int game_init()
{
	if (!game.actions)
		game.actions = array_create(RTG_TYPE_PTR);
	if (!game.players)
		game.players = array_create(RTG_TYPE_PTR);
	if (game.state)
		return 1;
	if (!game.thread) {
		game.thread = thread_create(game_main,NULL);
	}else{
		thread_wake(game.thread);
	}
	return 0;
}

void game_exit()
{
	if (!game.thread)
		return;

	game.state = 0;

	thread_wait(game.thread);
}

int game_push_action(action_t *a)
{
	playerdata_t *p;
	playerdata_t *e;
	if (!a)
		return 1;

	p = game_get_playerdata(a->owner);
	e = game_get_playerdata(a->target);
	if (!p)
		return 1;
	if (p->upgrading)
		return ACTERR_UPGRADE;
	if (p->defeated)
		return ACTERR_DEFEATED;

	switch (a->action) {
	case ACTION_ATTACK:
		if (!e || e->defeated)
			return ACTERR_NOTARGET;
		if (e->id == p->id)
			return ACTERR_NOTARGET;
		if (p->units[UNIT_SOLDIER] < a->units[UNIT_SOLDIER])
			return ACTERR_NOUNITS;
		if (p->units[UNIT_BRIGADIER] < a->units[UNIT_BRIGADIER])
			return ACTERR_NOUNITS;
		a->doin = 2;
		break;
	case ACTION_TRAIN:
		if (a->units[UNIT_VILLAGER]) {
			if (game_get_cost(p,UNIT_VILLAGER,a->units[UNIT_VILLAGER]) > p->credits)
				return ACTERR_NOCREDIT;
			a->doin = 1;
		}
		if (a->units[UNIT_FARMER]) {
			if (p->units[UNIT_VILLAGER] < a->units[UNIT_FARMER])
				return ACTERR_NOUNITS;
			if (game_get_cost(p,UNIT_FARMER,a->units[UNIT_FARMER]) > p->credits)
				return ACTERR_NOCREDIT;
			a->doin = 1;
		}
		if (a->units[UNIT_MASTER]) {
			if (p->units[UNIT_FARMER] < a->units[UNIT_MASTER])
				return ACTERR_NOUNITS;
			if (game_get_cost(p,UNIT_MASTER,a->units[UNIT_MASTER]) > p->credits)
				return ACTERR_NOCREDIT;
			a->doin = 3;
		}
		if (a->units[UNIT_SOLDIER]) {
			if (p->units[UNIT_VILLAGER] < a->units[UNIT_SOLDIER])
				return ACTERR_NOUNITS;
			if (game_get_cost(p,UNIT_SOLDIER,a->units[UNIT_SOLDIER]) > p->credits)
				return ACTERR_NOCREDIT;
			a->doin = 1;
		}
		if (a->units[UNIT_BRIGADIER]) {
			if (p->units[UNIT_SOLDIER] < a->units[UNIT_BRIGADIER])
				return ACTERR_NOUNITS;
			if (game_get_cost(p,UNIT_BRIGADIER,a->units[UNIT_BRIGADIER]) > p->credits)
				return ACTERR_NOCREDIT;
			a->doin = 3;
		}
		if (a->units[UNIT_SCOUT]) {
			if (p->units[UNIT_VILLAGER] < a->units[UNIT_SCOUT])
				return ACTERR_NOUNITS;
			if (game_get_cost(p,UNIT_SCOUT,a->units[UNIT_SCOUT]) > p->credits)
				return ACTERR_NOCREDIT;
			a->doin = 2;
		}
		break;
	case ACTION_UPGRADE:
		if (game_get_cost(p,ACTION_UPGRADE,0) > p->credits)
			return ACTERR_NOCREDIT;
		a->doin = game.difficulty*3;
		p->upgrading = 1;
		break;
	default:
		return 1;
	}

	return array_push_ptr(game.actions,a);
}


/* get the cost of training, or promoting a unit, or upgrading strength */
int game_get_cost(playerdata_t *p, int type, int count)
{
	int c;
	if (!count) {
		if (type == ACTION_UPGRADE) {
			if (p) {
				return (200*game.difficulty)*p->strength;
			}else{
				return 200*game.difficulty;
			}
		}
		return 0;
	}
	switch (type) {
		case UNIT_VILLAGER:
			c = 4*game.difficulty;
			break;
		case UNIT_FARMER:
			c = 8*game.difficulty;
			break;
		case UNIT_MASTER:
			c = 32*game.difficulty;
			break;
		case UNIT_SOLDIER:
			c = 16*game.difficulty;
			break;
		case UNIT_BRIGADIER:
			c = 64*game.difficulty;
			break;
		case UNIT_SCOUT:
			c = 24*game.difficulty;
			break;
		default:
			c = 10*game.difficulty;
	}
	return (c*count);
}
