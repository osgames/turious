/************************************************************************
* village.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

villagedata_t village = {
	0,
	0,
	0,
	NULL,
	NULL,
	NULL,
	0.0,
	0.0,
	30,
	30,
	0,
	0,
	NULL
};

static void vcb_vc(int x, int y)
{
	ui_style("town.!.*","visible","true");
}

static void vcb_barracks(int x, int y)
{
	ui_style("barracks.!.*","visible","true");
}

static void vcb_farm(int x, int y)
{
	ui_style("farm.!.*","visible","true");
}

static void vcb_click(int x, int y)
{
	rtprintf("click: (%d,%d)",x,y);
}

static void (*village_callbacks[])(int x, int y) = {
	vcb_click,
	vcb_vc,
	vcb_barracks,
	vcb_farm
};

static void village_update_unitpos()
{
	village_unit_t *u;
	int bx;
	int by;
	int ex;
	int ey;

	u = village.units;
	while (u) {
		village_to_screen(u->pos[0],u->pos[1],&bx,&by);
		village_to_screen(u->target[0],u->target[1],&ex,&ey);
		u->anim->bx = bx;
		u->anim->by = by;
		u->anim->ex = ex;
		u->anim->ey = ey;
		u = u->next;
	}
}

static void village_load_sprites()
{
	village.ground = malloc(sizeof(animation_t*)*4);
	village.ground[0] = anim_create("tiles.png",16,32,64,1,7,0);
	village.ground[0]->id = -1;
	village.ground[1] = anim_create("overlay.png",16,96,64,1,3,0);
	village.ground[1]->id = -1;
	village.ground[2] = anim_create("trees.png",4,256,256,1,4,0);
	village.ground[2]->id = -1;
	village.ground[3] = anim_create("structures.png",4,256,256,1,4,0);
	village.ground[3]->id = -1;

	village.unit = malloc(sizeof(animation_t*)*25);
	village.unit[0] = anim_create("unit_state.png",4,128,128,2,1,0);
	village.unit[0]->id = -1;
	/* villager */
	village.unit[1] = anim_create("villager_stand.png",1,128,128,3,8,0.01);
	village.unit[1]->id = (UNIT_VILLAGER | UACT_STAND);
	village.unit[2] = anim_create("villager_walk.png",15,128,128,5,8,0.01);
	village.unit[2]->id = (UNIT_VILLAGER | UACT_MOVE);
	village.unit[3] = anim_create("villager_tend.png",13,128,128,5,8,0.01);
	village.unit[3]->id = (UNIT_VILLAGER | UACT_TEND);
	village.unit[4] = anim_create("villager_attack.png",6,128,128,5,8,0.01);
	village.unit[4]->id = (UNIT_VILLAGER | UACT_ATTACK);
	/* farmer */
	village.unit[5] = anim_create("farmer_stand.png",4,128,128,3,8,0.01);
	village.unit[5]->id = (UNIT_FARMER | UACT_STAND);
	village.unit[6] = anim_create("farmer_walk.png",15,128,128,3,8,0.01);
	village.unit[6]->id = (UNIT_FARMER | UACT_MOVE);
	village.unit[7] = anim_create("farmer_tend.png",13,128,128,5,8,0.01);
	village.unit[7]->id = (UNIT_FARMER | UACT_TEND);
	village.unit[8] = anim_create("farmer_attack.png",6,128,128,5,8,0.01);
	village.unit[8]->id = (UNIT_FARMER | UACT_ATTACK);
	/* master farmer */
	village.unit[9] = anim_create("master_farmer_stand.png",4,128,128,3,8,0.01);
	village.unit[9]->id = (UNIT_MASTER | UACT_STAND);
	village.unit[10] = anim_create("master_farmer_walk.png",15,128,128,3,8,0.01);
	village.unit[10]->id = (UNIT_MASTER | UACT_MOVE);
	village.unit[11] = anim_create("master_farmer_tend.png",13,128,128,5,8,0.01);
	village.unit[11]->id = (UNIT_MASTER | UACT_TEND);
	village.unit[12] = anim_create("master_farmer_attack.png",6,128,128,5,8,0.01);
	village.unit[12]->id = (UNIT_MASTER | UACT_ATTACK);
	/* soldier */
	village.unit[13] = anim_create("soldier_stand.png",4,128,128,3,8,0.01);
	village.unit[13]->id = (UNIT_SOLDIER | UACT_STAND);
	village.unit[14] = anim_create("soldier_walk.png",15,128,128,3,8,0.01);
	village.unit[14]->id = (UNIT_SOLDIER | UACT_MOVE);
	village.unit[15] = anim_create("soldier_tend.png",13,128,128,5,8,0.01);
	village.unit[15]->id = (UNIT_SOLDIER | UACT_TEND);
	village.unit[16] = anim_create("soldier_attack.png",6,128,128,5,8,0.01);
	village.unit[16]->id = (UNIT_SOLDIER | UACT_ATTACK);
	/* brigadier */
	village.unit[17] = anim_create("brigadier_stand.png",4,128,128,3,8,0.01);
	village.unit[17]->id = (UNIT_BRIGADIER | UACT_STAND);
	village.unit[18] = anim_create("brigadier_walk.png",15,128,128,3,8,0.01);
	village.unit[18]->id = (UNIT_BRIGADIER | UACT_MOVE);
	village.unit[19] = anim_create("brigadier_tend.png",13,128,128,5,8,0.01);
	village.unit[19]->id = (UNIT_BRIGADIER | UACT_TEND);
	village.unit[20] = anim_create("brigadier_attack.png",6,128,128,5,8,0.01);
	village.unit[20]->id = (UNIT_BRIGADIER | UACT_ATTACK);
	/* scout */
	village.unit[21] = anim_create("scout_stand.png",4,128,128,3,8,0.01);
	village.unit[21]->id = (UNIT_SCOUT | UACT_STAND);
	village.unit[22] = anim_create("scout_walk.png",15,128,128,3,8,0.01);
	village.unit[22]->id = (UNIT_SCOUT | UACT_MOVE);
	village.unit[23] = anim_create("scout_tend.png",13,128,128,5,8,0.01);
	village.unit[23]->id = (UNIT_SCOUT | UACT_TEND);
	village.unit[24] = anim_create("scout_attack.png",6,128,128,5,8,0.01);
	village.unit[24]->id = (UNIT_SCOUT | UACT_ATTACK);
}

static void village_draw_info(int x, int y, village_unit_t *u)
{
}

void village_select()
{
	int x;
	int y;
	int h;
	tile_t *t;

	h = village_from_screen(current_event.x,current_event.y,&x,&y);
	if (!h)
		return;

	t = village_get_tile(x,y);
	if (t && village_callbacks[t->click])
		village_callbacks[t->click](x,y);
}

void village_move_up()
{
	village.centre_y -= anim_factor;

	if (village.centre_y < 0)
		village.centre_y = 0;
}
void village_move_down()
{
	village.centre_y += anim_factor;

	if (village.centre_y > village.y-village.draw_height)
		village.centre_y = village.y-village.draw_height;
}
void village_move_left()
{
	village.centre_x -= anim_factor;

	if (village.centre_x < 0)
		village.centre_x = 0;
}
void village_move_right()
{
	village.centre_x += anim_factor;

	if (village.centre_x > village.x-village.draw_width)
		village.centre_x = village.x-village.draw_width;
}

void village_set(int x, int y, int bt, int b, int ot, int o, int c, int s)
{
	if (village.state < 2)
		return;

	village.tiles[x][y].base[0] = bt;
	village.tiles[x][y].base[1] = b;
	village.tiles[x][y].overlay[0] = ot;
	village.tiles[x][y].overlay[1] = o;
	village.tiles[x][y].click = c;
	village.tiles[x][y].shade = s;
}

void village_load(int x, int y)
{
	int px;
	int py;

	if (!village.state)
		village.state = 1;

	village.draw_height = (wm_res.width/43);
	village.draw_width = village.draw_height;

	if (x != village.x && y != village.y) {
		village.centre_x = 0;
		village.centre_y = 0;
		village.x = x;
		village.y = y;
		village.tiles = malloc(sizeof(tile_t*)*village.x);
		for (px=0; px<village.x; px++) {
			village.tiles[px] = malloc(sizeof(tile_t)*village.y);
			for (py=0; py<village.y; py++) {
				village.tiles[px][py].base[0] = -1;
				village.tiles[px][py].overlay[0] = -1;
				village.tiles[px][py].click = 0;
			}
		}
	}

	village.state = 2;
}

void village_unload()
{
	village.state = 1;
	if (village.tiles) {
		int k;
		for (k=0; k<village.x; k++) {
			free(village.tiles[k]);
		}
		free(village.tiles);
		village.tiles = NULL;
	}
	village.x = 0;
	village.y = 0;
	village.state = 0;
}

void village_draw()
{
	int min_x = village.centre_x;
	int min_y = village.centre_y;
	int max_x = village.centre_x+village.draw_width;
	int max_y = village.centre_y+village.draw_height;
	int bx = wm_res.width/2;
	int dx;
	int dy;
	int x;
	int y;
	int sx;
	int sy;
	int px;
	int py;
	int mx = -1;
	int my = -1;
	village_unit_t *u;
	color_t shade = {128,128,128,255};

	if (!village.ground)
		village_load_sprites();

	if (village.state < 2)
		return;

	village_update_unitpos();

	village_from_screen(mouse[CUR_X],mouse[CUR_Y],&mx,&my);

	/* draw the tiles first */
	for (x=min_x,px=0; x<max_x; x++,px++) {
		if (x < 0 || x >= village.x)
			continue;
		dx = bx+(32*px);
		dy = (16*px)-64;
		for (y=min_y,py=0; y<max_y; y++,py++) {
			if (y < 0 || y >= village.y)
				continue;
			if (dx > -128 && dy > -128 && dx < wm_res.width && dy < wm_res.height) {
				if (village.tiles[x][y].shade == SHADE_DARK)
					render_color_push(&shade);
				if (village.tiles[x][y].base[0] > -1) {
					sy = village.tiles[x][y].base[1]/16;
					sx = village.tiles[x][y].base[1]%16;
					draw2d_sprite(&village.ground[0]->frame_data[sy][sx],dx,dy+64,64,32);
				}
				if (x == mx && y == my)
					draw2d_sprite(&village.ground[0]->frame_data[6][0],dx,dy+64,64,32);
				if (village.tiles[x][y].shade == SHADE_DARK)
					render_color_pop();
			}
			dx -= 32;
			dy += 16;
		}
	}

	/* then the overlays and units */
	for (x=min_x,px=0; x<max_x; x++,px++) {
		if (x < 0 || x >= village.x)
			continue;
		dx = bx+(32*px);
		dy = (16*px)-64;
		for (y=min_y,py=0; y<max_y; y++,py++) {
			if (y < 0 || y >= village.y)
				continue;
			if (village.tiles[x][y].overlay[0] > -1) {
				if (village.tiles[x][y].shade == SHADE_DARK)
					render_color_push(&shade);
				if (village.tiles[x][y].overlay[0] < 2) {
					sy = village.tiles[x][y].overlay[1]/16;
					sx = village.tiles[x][y].overlay[1]%16;
					draw2d_sprite(&village.ground[village.tiles[x][y].overlay[0]]->frame_data[sy][sx],dx,dy,64,96);
				}else if (village.tiles[x][y].overlay[0] == 3) {
					sy = village.tiles[x][y].overlay[1]/4;
					sx = village.tiles[x][y].overlay[1]%4;
					draw2d_sprite(&village.ground[3]->frame_data[sy][sx],dx-96,dy-80,256,256);
				}else{
					if (x > mx-2 && x < mx+5 && y > my-2 && y < my+5) {
						sy = (village.tiles[x][y].overlay[1]+8)/4;
						sx = (village.tiles[x][y].overlay[1]+8)%4;
					}else{
						sy = village.tiles[x][y].overlay[1]/4;
						sx = village.tiles[x][y].overlay[1]%4;
					}
					draw2d_sprite(&village.ground[2]->frame_data[sy][sx],dx-96,dy-160,256,256);
				}
				if (village.tiles[x][y].shade == SHADE_DARK)
					render_color_pop();
			}
			u = village_unit_at(x,y);
			if (u) {
				int ax;
				int ay;
				anim_get_position(u->anim,&ax,&ay);
				anim_draw(u->anim,ax,ay-76);
				if (u->owner == client.cid) {
					sx = (u->action>>4);
				}else{
					sx = 3;
				}
				draw2d_sprite(&village.unit[0]->frame_data[0][sx],ax,ay-76,128,128);
				/* switch to a standing animation once the unit has reached its destination */
				if (u->action == UACT_MOVE && u->anim->ex == ax && u->anim->ey == ay && !u->reset) {
					u->reset = 1;
					u->anim->loop = 0;
					u->anim->u = 0;
					u->anim = anim_spawn((u->type|UACT_STAND),u->anim->alt,-1,ax,ay,ax,ay);
				}
				if (x == mx && y == my)
					village_draw_info(ax,ay,u);
			}
			dx -= 32;
			dy += 16;
		}
	}
}

int village_to_screen(int x, int y, int *sx, int *sy)
{
	y -= village.centre_y;
	x -= village.centre_x;

	*sx = (((wm_res.width/2)-32)+(32*x))-(32*y);
	*sy = (16*y)+(16*x);

	return 0;
}

int village_from_screen(int sx, int sy, int *x, int *y)
{
	int c;
	int r;
	int cm;
	int rm;
	tile_t *t;
	sx -= ((wm_res.width/2)+32);
	sy -= 0;

	c = ((64*sy)-(32*sx));
	r = ((64*sy)+(32*sx));

	cm = c%2048;
	rm = r%2048;

	c /= 2048;
	r /= 2048;

	if (c == 0 && cm <0)
		c--;
	if (r == 0 && rm <0)
		r--;

	c += village.centre_y;
	r += village.centre_x;

	t = village_get_tile(r,c);
	if (t && t->base[0] < 0 && t->overlay[0] < 0)
		return 0;

	*y = c;
	*x = r;

	return 1;
}

tile_t *village_get_tile(int x, int y)
{
	if (village.state < 2 || x<0 || y<0 || x >= village.x || y >= village.y)
		return NULL;

	return &village.tiles[x][y];
}

void village_goto(int x, int y)
{
	village.centre_x = (x-(village.draw_width/2));
	village.centre_y = (y-(village.draw_height/2));

	if (village.centre_x < 0)
		village.centre_x = 0;
	if (village.centre_x > village.x-village.draw_width)
		village.centre_x = village.x-village.draw_width;

	if (village.centre_y < 0)
		village.centre_y = 0;
	if (village.centre_y > village.y-village.draw_height)
		village.centre_y = village.y-village.draw_height;
}

void village_set_home(int x, int y)
{
	village.home_x = x;
	village.home_y = y;
	village_goto(x,y);
}

int village_get_home()
{
	return (village.home_y*village.x)+village.home_x;
}

void village_gohome()
{
	village.centre_x = village.home_x-(village.draw_width/2);
	village.centre_y = village.home_y-(village.draw_height/2);
}

village_unit_t *village_unit(int id)
{
	village_unit_t *u = village.units;
	while (u) {
		if (u->id == id)
			return u;
		u = u->next;
	}
	return NULL;
}

village_unit_t *village_unit_at(int x, int y)
{
	village_unit_t *u = village.units;
	while (u) {
		if (u->action == UACT_MOVE && (u->target[0] > u->pos[0] || u->target[1] > u->pos[1]) && u->target[0] == x && u->target[1] == y)
			return u;
		if (u->pos[0] == x && u->pos[1] == y)
			return u;
		u = u->next;
	}
	return NULL;
}
