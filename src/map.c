/************************************************************************
* map.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

static struct {
	int state;
	int visible;
	mapnode_t *raw;
	image_t px;
	material_t *mat;
	float x_off;
	float y_off;
} map = {
	0,
	0,
	NULL,
	{
		0,
		0,
		NULL
	},
	NULL,
	0.0,
	0.0
};

void map_select()
{
	int x;
	int y;
	int h = map_from_screen(current_event.x,current_event.y,&x,&y);

	rtprintf("x:%d y:%d (%d)",x,y,h);
}
void map_toggle()
{
	map.visible = !map.visible;
	if (map.visible) {
		cmd_execf("exec ui.elements(map.!.*.style.visible) true");
	}else{
		cmd_execf("exec ui.elements(map.!.*.style.visible) false");
	}
}
int map_toggle_ui(widget_t *w)
{
	map_toggle();
	return EVENT_HANDLED;
}
void map_move_up()
{
	map.y_off += anim_factor*3.0;
}
void map_move_down()
{
	map.y_off -= anim_factor*3.0;
}
void map_move_left()
{
	map.x_off += anim_factor*3.0;
}
void map_move_right()
{
	map.x_off -= anim_factor*3.0;
}

void map_load(mapnode_t *data, int w, int h)
{
	array_t *a;
	int l;
	int i;
	int k;
	drawspec_t *t;
	drawspec_t *d;

	rtprintf(CN_DEBUG "map_load");

	if (!map.state)
		map.state = 1;

	village_load(w,h);
	l = w*h;
	if (w != map.px.w || h != map.px.h) {
		map.px.w = w;
		map.px.h = h;
		l *= 4;
		if (map.px.pixels)
			free(map.px.pixels);
		map.px.pixels = malloc(l);
		for (i=0; i<l; i+=4) {
			map.px.pixels[i] = 0;
			map.px.pixels[i+1] = 0;
			map.px.pixels[i+2] = 0;
			map.px.pixels[i+3] = 0;
		}
		if (map.raw)
			free(map.raw);
		map.raw = malloc(l);
		l = map.px.w*map.px.h;
	}

	memcpy(map.raw,data,l*4);

	for (i=0,k=0; i<l; i++,k+=4) {
		t = drawspec_get(map.raw[i].tile);
		d = drawspec_get(map.raw[i].detail);

		if (map.raw[i].detail == MAP_BLANK) {
			map.px.pixels[k] = t->c.r;
			map.px.pixels[k+1] = t->c.g;
			map.px.pixels[k+2] = t->c.b;
			map.px.pixels[k+3] = t->c.a;
		}else{
			map.px.pixels[k] = d->c.r;
			map.px.pixels[k+1] = d->c.g;
			map.px.pixels[k+2] = d->c.b;
			map.px.pixels[k+3] = d->c.a;
		}

		village_set(i%map.px.w,i/map.px.w,t->texture,t->sprite,d->texture,d->sprite,d->click,map.raw[i].shade);
		if (map.state != 2 && map.raw[i].detail == (MAP_VC1|MAP_OWN))
			village_set_home(i%map.px.w,i/map.px.w);
	}

	map.state = 2;

	map.mat = mat_update_pixels(map.mat,&map.px);
	a = style_select("map.label.image");
	if (a) {
		if (a->length) {
			float s;
			widget_t *w = array_get_ptr(a,0);
			ui_widget_setbg_sprite(
				w,
				map.mat,
				0,
				0,
				map.px.w,
				map.px.h
			);
			s = ((float)w->parent->style.h-70.0)/(float)w->style.h;
			w->style.w = map.px.w*s;
			w->style.h = map.px.h*s;
		}
		array_free(a);
	}
}

void map_unload()
{
	map.state = 1;
	if (map.raw) {
		free(map.raw);
		map.raw = NULL;
	}
	if (map.px.pixels) {
		free(map.px.pixels);
		map.px.pixels = NULL;
	}
	map.px.h = 0;
	map.px.w = 0;
	map.state = 0;

	village_unload();
}

int map_from_screen(int sx, int sy, int *x, int *y)
{
	sx -= (wm_res.width/2)-(map.px.w/2);
	sy -= (wm_res.height/2)-(map.px.h/2);

	*y = sx;
	*x = sy;

	return 1;
}

mapnode_t *map_get_node(int x, int y)
{
	if (map.state < 2 || x<0 || y<0 || x >= map.px.w || y >= map.px.h)
		return NULL;

	return &map.raw[x+(y*map.px.w)];
}
