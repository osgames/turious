/************************************************************************
* scout.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

void scout_notify(playerdata_t *p, playerdata_t *e, int event)
{
	if (event == ACTION_TRAIN) {
		int i;
		playerdata_t *t;
		int m = player_count();
		for (i=0; i<m; i++) {
			if ((i+1) == p->id || player_is_ai(i+1))
				continue;
			t = game_get_playerdata(i+1);
			if (!t)
				continue;
			/* if the player has scouts */
			if (t->units[UNIT_SCOUT] > 0) {
				/* either display a message */
				if (rand_range(0,t->units[UNIT_SCOUT])) {
					player_signal(t->id,"'%s' is training a large army",player_name(p->id));
				}else{
					/* or kill a scout */
					t->units[UNIT_SCOUT]--;
				}
			}
		}
	}else if (event == ACTION_ATTACK) {
		/* if the player has scouts */
		if (!player_is_ai(e->id) && e->units[UNIT_SCOUT] > 0) {
			/* either display a message */
			if (rand_range(0,e->units[UNIT_SCOUT])) {
				player_signal(e->id,"An attack is imminent!");
			}else{
				/* or kill a scout */
				e->units[UNIT_SCOUT]--;
			}
		}
	}
}
