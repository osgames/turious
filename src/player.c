/************************************************************************
* player.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

#include <stdarg.h>

typedef struct player_s {
	char* name;
	int id;
	comset_t *c;
} player_t;

static array_t *players = NULL;

char* acterrors[] = {
	NULL,
	"Error: Something bad happened",
	"You can't do that while upgrading your strength",
	"You don't have enough units to do that",
	"You don't have enough credit to do that",
	"Who? That target doesn't exist",
	"You can't do that, you've been defeated"
};

int player_connect(char* name, comset_t *c)
{
	int id = -1;
	player_t *cl = malloc(sizeof(player_t));

	if (!players)
		players = array_create(RTG_TYPE_PTR);

	cl->c = c;
	if (name && name[0])
		cl->name = strdup(name);

	if (players->length) {
		player_t *p;
		int i;
		for (i=0; i<players->length; i++) {
			p = array_get_ptr(players,i);
			if (!p) {
				array_set_ptr(players,cl,i);
				id = i+1;
				break;
			}
		}
	}

	if (id < 0) {
		array_push_ptr(players,cl);
		id = players->length;
	}

	cl->id = id;

	if (!name || !name[0]) {
		char n[100];
		sprintf(n,"Player %d",id);
		cl->name = strdup(n);
	}

	return id;
}

void player_disconnect(int id)
{
	int i;
	int p;
	action_t *a;
	player_t *cl = array_get_ptr(players,id-1);
	if (!cl)
		return;

	a = malloc(sizeof(action_t));
	a->owner = id;
	a->action = ACTION_QUIT;
	game_push_action(a);

	array_set_ptr(players,NULL,id-1);

	free(cl->c);
	free(cl->name);
	free(cl);

	p = 0;
	for (i=0; i<players->length; i++) {
		cl = array_get_ptr(players,i);
		if (!cl)
			continue;
		if (cl->c->type == CLIENT_HUMAN) {
			p = 1;
			break;
		}
	}

	if (!p)
		game_exit();
}

void player_unit(int id, int owner, int unit, int uid, int hp, int action, array_t *path)
{
	player_t *cl;
	int *t;

	if (id < 0) {
		int i;
		for (i=0; i<players->length; i++) {
			cl = array_get_ptr(players,i);
			if (!cl)
				continue;
			if (!hp || owner == cl->id) {
				player_unit(cl->id,owner,unit,uid,hp,action,path);
			}else{
				t = array_get_ptr(path,0);
				if (t && shade_get(cl->id,t[0],t[1]) == SHADE_BRIGHT) {
					player_unit(cl->id,owner,unit,uid,hp,action,path);
				}else{
					player_unit(cl->id,owner,unit,uid,0,action,path);
				}
			}
		}
		return;
	}

	cl = array_get_ptr(players,id-1);
	if (!cl || !cl->c->unit)
		return;

	cl->c->unit(id,owner,unit,uid,hp,action,path);
}

void player_message(int id, char* str, ...)
{
	player_t *cl;
	char msg[1024];
	va_list ap;

	cl = array_get_ptr(players,id-1);
	if (!cl || !cl->c->msg)
		return;

	va_start(ap, str);
	vsnprintf(msg, 1024, str, ap);
	va_end(ap);

	cl->c->msg(id,msg);
}

void player_message_all(char* str, ...)
{
	player_t *cl;
	char msg[1024];
	va_list ap;
	int i;

	msg[0] = '\r';

	va_start(ap, str);
	vsnprintf(msg+1, 1023, str, ap);
	va_end(ap);


	for (i=0; i<players->length; i++) {
		cl = array_get_ptr(players,i);
		if (cl && cl->c->msg)
			cl->c->msg(i+1,msg);
	}
}

void player_signal(int id, char* str, ...)
{
	player_t *cl;
	char msg[1024];
	va_list ap;

	cl = array_get_ptr(players,id-1);
	if (!cl || !cl->c->sig)
		return;

	va_start(ap, str);
	vsnprintf(msg, 1024, str, ap);
	va_end(ap);

	cl->c->sig(id,msg);
}

void player_update(int id, int c, int s, int u[UNIT_COUNT])
{
	player_t *cl = array_get_ptr(players,id-1);
	if (!cl || !cl->c->data)
		return;

	cl->c->data(id,c,s,u);
}

void player_map(int id, mapnode_t *data, int w, int h)
{
	player_t *cl = array_get_ptr(players,id-1);
	if (!cl || !cl->c->map)
		return;

	cl->c->map(id,data,w,h);
}

void player_state(int id, int state)
{
	player_t *cl = array_get_ptr(players,id-1);
	if (!cl || !cl->c->state)
		return;

	cl->c->state(id,state);
}

void player_init(int id, int vg_c, int fm_c, int mf_c, int sd_c, int bg_c, int sc_c, int ug_c, array_t *player_names)
{
	player_t *cl = array_get_ptr(players,id-1);
	if (!cl || !cl->c->init)
		return;

	cl->c->init(id,vg_c,fm_c,mf_c,sd_c,bg_c,sc_c,ug_c,players);
}

char* player_name(int id)
{
	player_t *cl = array_get_ptr(players,id-1);
	if (!cl || !cl->name)
		return "";

	return cl->name;
}

int player_is_ai(int id)
{
	player_t *cl = array_get_ptr(players,id-1);
	if (cl && cl->c->type == CLIENT_AI)
		return 1;

	return 0;
}

int player_is_defeated(int id)
{
	playerdata_t *p = game_get_playerdata(id);
	if (!p)
		return 0;

	return p->defeated;
}

int player_count()
{
	player_t *p;
	int i;
	int c = 0;
	if (!players)
		return 0;

	for (i=0; i<players->length; i++) {
		p = array_get_ptr(players,i);
		if (p)
			c++;
	}

	return c;
}
