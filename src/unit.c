/************************************************************************
* unit.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2014 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

static unit_t *units = NULL;
static int unit_ids = 1;
/*
typedef struct unit_s {
	struct unit_s *prev;
	struct unit_s *next;
	int id;
	int owner;
	int type;
	int hp;
	int pos;
	int step;
	int focus;
	int action;
	array_t *path;
} unit_t;
*/
/*static int unit_find_space(int s)
{
	return s;
}*/

static int unit_find_tree(int x, int y, int radius, int x_m, int y_m, int x_M, int y_M, int *tx, int *ty)
{
	int tree;
	int ix;
	int iy;
	int x_t;
	int y_t;
	int xr_m = x-radius;
	int yr_m = y-radius;
	int xr_M = x+radius;
	int yr_M = y+radius;

	for (ix=xr_m; ix<=xr_M; ix++) {
		x_t = ix;
		if (ix < x_m)
			x_t = x_m;
		if (ix > x_M)
			x_t = x_M;
		for (iy=yr_m; iy<=yr_M; iy++) {
			y_t = iy;
			if (iy < y_m)
				y_t = y_m;
			if (iy > y_M)
				y_t = y_M;
			tree = mapgen_get_detail(x_t,y_t);
			if (
				tree == MAP_GRASSTREE1
				|| tree == MAP_GRASSTREE2
				|| tree == MAP_GRASSTREE3
				|| tree == MAP_GRASSTREE4
				|| tree == MAP_SANDTREE1
				|| tree == MAP_SANDTREE2
				|| tree == MAP_SANDTREE3
				|| tree == MAP_SANDTREE4
			) {
				*tx = x_t;
				*ty = y_t;
				return tree;
			}
		}
	}
	return 0;
}

static int unit_find_farm(int pid)
{
	return mapgen_find(pid,MAP_FARM1);
}

/*static int unit_find_farmspace(playerdata_t *p)
{
	return 0;
}*/

static void unit_spawn_villager(playerdata_t *p, unit_t *u)
{
	int start;
	int *t;
	int x_c;
	int y_c;
	int x;
	int y;
	int tl;

	start = mapgen_find(p->id,MAP_VC1);
	x_c = start%mapgen.w;
	y_c = start/mapgen.w;
	do {
		x = rand_range(x_c-10,x_c+10);
		y = rand_range(y_c-10,y_c+10);
		tl = mapgen_get_tile(x,y);
	} while (distance(x,y,x_c,y_c) < 3 || tl == MAP_WATER || tl == MAP_BLANK);

	u->path = path_find(x_c,y_c,x,y);
	u->pos = x_c+(y_c*mapgen.w);
	t = array_get_ptr(u->path,1);
	u->target = t[0]+(t[1]*mapgen.w);
	u->action = UACT_MOVE;

	player_unit(-1,p->id,UNIT_VILLAGER,u->id,u->hp,u->action,u->path);
	for (x=-2; x<3; x++) {
		for (y=-2; y<3; y++) {
			shade_set(u->owner,t[0]+x,t[1]+y,SHADE_BRIGHT);
		}
	}
}

static void unit_spawn_farmer(playerdata_t *p, unit_t *u)
{
	int start;
	int target;
	int *t;

	target = unit_find_farm(p->id);
	start = u->pos;
	u->path = path_find(start%mapgen.w,start/mapgen.w,target%mapgen.w,target/mapgen.w);
	if (start != target) {
		t = array_get_ptr(u->path,1);
		u->target = t[0]+(t[1]*mapgen.w);
		u->action = UACT_MOVE;
	}else{
		u->target = start;
	}
	player_unit(-1,p->id,UNIT_FARMER,u->id,u->hp,u->action,u->path);
}

static void unit_spawn_master(playerdata_t *p, unit_t *u)
{
	int start;
	int target;
	int *t;

	target = unit_find_farm(p->id);
	start = u->pos;
	u->path = path_find(start%mapgen.w,start/mapgen.w,target%mapgen.w,target/mapgen.w);
	if (start != target) {
		t = array_get_ptr(u->path,1);
		u->target = t[0]+(t[1]*mapgen.w);
		u->action = UACT_MOVE;
	}else{
		u->target = start;
	}
	player_unit(-1,p->id,UNIT_MASTER,u->id,u->hp,u->action,u->path);
}

static void unit_spawn_soldier(playerdata_t *p, unit_t *u)
{
	int start;
	int target;
	int *t;

	target = mapgen_find(p->id,MAP_BARRACKS1);
	start = u->pos;
	u->path = path_find(start%mapgen.w,start/mapgen.w,target%mapgen.w,target/mapgen.w);
	if (start != target) {
		t = array_get_ptr(u->path,1);
		u->target = t[0]+(t[1]*mapgen.w);
		u->action = UACT_MOVE;
	}else{
		u->target = start;
	}
	player_unit(-1,p->id,UNIT_SOLDIER,u->id,u->hp,u->action,u->path);
}

static void unit_spawn_brigadier(playerdata_t *p, unit_t *u)
{
	int start;
	int target;
	int *t;

	target = mapgen_find(p->id,MAP_BARRACKS1);
	start = u->pos;
	u->path = path_find(start%mapgen.w,start/mapgen.w,target%mapgen.w,target/mapgen.w);
	if (start != target) {
		t = array_get_ptr(u->path,1);
		u->target = t[0]+(t[1]*mapgen.w);
		u->action = UACT_MOVE;
	}else{
		u->target = start;
	}
	player_unit(-1,p->id,UNIT_BRIGADIER,u->id,u->hp,u->action,u->path);
}

static void unit_spawn_scout(playerdata_t *p, unit_t *u)
{
	int start;
	int target;
	int *t;

	target = mapgen_find(p->id,MAP_BARRACKS1);
	start = u->pos;
	u->path = path_find(start%mapgen.w,start/mapgen.w,target%mapgen.w,target/mapgen.w);
	if (start != target) {
		t = array_get_ptr(u->path,1);
		u->target = t[0]+(t[1]*mapgen.w);
		u->action = UACT_MOVE;
	}else{
		u->target = start;
	}
	player_unit(-1,p->id,UNIT_SCOUT,u->id,u->hp,u->action,u->path);
}

void unit_spawn(playerdata_t *p, int type, int count)
{
	unit_t *u;
	int i;

	if (type == UNIT_VILLAGER) {
		for (i=0; i<count; i++) {
			u = malloc(sizeof(unit_t));
			u->id = unit_ids++;
			u->owner = p->id;
			u->type = type;
			u->hp = 100;
			u->pos = -1;
			u->step = 0;
			u->focus = -1;
			u->action = UACT_STAND;
			u->path = NULL;
			unit_spawn_villager(p,u);
			units = list_push(&units,u);
		}
		return;
	}

	for (i=0; i<count; i++) {
		u = unit_find_villager(p->id);
		u->type = type;
		u->hp = 100;
		u->pos = u->target;
		u->target = -1;
		u->step = 0;
		u->focus = -1;
		u->action = UACT_STAND;
		path_free(u->path);
		u->path = NULL;
		switch (type) {
		case UNIT_FARMER:
			unit_spawn_farmer(p,u);
			break;
		case UNIT_MASTER:
			unit_spawn_master(p,u);
			break;
		case UNIT_SOLDIER:
			unit_spawn_soldier(p,u);
			break;
		case UNIT_BRIGADIER:
			unit_spawn_brigadier(p,u);
			break;
		case UNIT_SCOUT:
			unit_spawn_scout(p,u);
			break;
		default:
			break;
		}
	}
}

void unit_step_villager(unit_t *u, playerdata_t *o)
{
	unit_t *e;
	int *t;
	int x;
	int y;
	int i;
	int ix;
	int iy;
	/* check that attacking units are attacking an enemy */
	if (u->action == UACT_ATTACK) {
		e = unit_find_pos(u->target);
		if (!e || e->owner == u->owner) {
			u->action = UACT_MOVE;
		}else{
			/* TODO: combat */
		}
	}else{
		x = u->pos%mapgen.w;
		y = u->pos/mapgen.w;
		if (u->action == UACT_MOVE) {
			x = u->target%mapgen.w;
			y = u->target/mapgen.w;
		}
		e = NULL;
		i = 2;
		/* look for an enemy unit nearby */
		for (ix=-(i-1); !e && ix<i; ix++) {
			for (iy=-(i-1); !e && iy<i; iy++) {
				if (!ix && !iy)
					continue;
				e = unit_find_pos((x+ix)+((y+iy)*mapgen.w));
				if (e && e->owner == u->owner)
					e = NULL;
			}
		}
		/* attack if found */
		if (e) {
			path_free(u->path);
			u->path = path_find(x,y,x+ix,y+iy);
			if (u->path->length > 2) {
				u->step = -1;
				u->action = UACT_MOVE;
				t = array_get_ptr(u->path,0);
				u->pos = t[0]+(t[1]*mapgen.w);
				t = array_get_ptr(u->path,1);
				u->target = t[0]+(t[1]*mapgen.w);
			}else{
				u->action = UACT_ATTACK;
				u->target = (x+ix)+((y+iy)*mapgen.w);
			}
		/* if the unit is standing around, find them something to do */
		}else if (u->action == UACT_STAND) {
			/* villager - either go somewhere at random, or go build a path/outpost/wall */
			/* go somewhere at random */
			if (o->has_trees || (o->has_paths && o->has_outposts && o->has_walls)) {
				int x_c;
				int y_c;
				int tx;
				int ty;
				int tree = 0;
				int skip = 0;
				int centre = mapgen_find(o->id,MAP_VC1);
				x_c = centre%mapgen.w;
				y_c = centre/mapgen.w;
				if (!player_is_ai(o->id))
				rtprintf(CN_DEBUG "1: %d: %d %d",u->id,x,y);
				/* try to find a tree */
				if (o->has_trees) {
				if (!player_is_ai(o->id))
				rtprintf(CN_DEBUG "2: %d: %d %d",u->id,x,y);
					/* find a tree */
					if (distance(x,y,x_c,y_c) < 18) {
						for (ix=-1; !tree && ix<2; ix++) {
							for (iy=-1; !tree && iy<2; iy++) {
								tree = mapgen_get_detail(x+ix,y+iy);
								if (
									tree == MAP_GRASSTREE1
									|| tree == MAP_GRASSTREE2
									|| tree == MAP_GRASSTREE3
									|| tree == MAP_GRASSTREE4
									|| tree == MAP_SANDTREE1
									|| tree == MAP_SANDTREE2
									|| tree == MAP_SANDTREE3
									|| tree == MAP_SANDTREE4
								) {
									e = unit_find_target((x+ix)+((y+iy)*mapgen.w),u->id);
				if (e && !player_is_ai(o->id))
				rtprintf(CN_DEBUG "2a: %d: %d %d",u->id,x,y);
									if (!e || e->type != UNIT_VILLAGER || e->action != UACT_TEND) {
										tx = x+ix;
										ty = y+iy;
										break;
									}
								}
								tree = 0;
							}
						}
					}
					if (tree) {
				if (!player_is_ai(o->id))
				rtprintf(CN_DEBUG "3: %d: %d %d",u->id,x,y);
						u->action = UACT_TEND;
						u->step = -1;
						path_free(u->path);
						u->path = array_create(RTG_TYPE_PTR);
						t = malloc(sizeof(int)*2);
						t[0] = x;
						t[1] = y;
						array_push_ptr(u->path,t);
						t = malloc(sizeof(int)*2);
						t[0] = tx;
						t[1] = ty;
						array_push_ptr(u->path,t);
						u->pos = u->target;
						u->target = tx+(ty*mapgen.w);
					}else{
						int x_m = x_c-16;
						int y_m = y_c-16;
						int x_M = x_c+16;
						int y_M = y_c+16;
				if (!player_is_ai(o->id))
				rtprintf(CN_DEBUG "4: %d: %d %d",u->id,x,y);
						for (i=distance(x,y,x_c,y_c); i<16 && !tree; i++) {
							tree = unit_find_tree(x,y,i,x_m,y_m,x_M,y_M,&ix,&iy);
							if (!tree)
								continue;
							if (tree && x == ix && y == iy) {
								skip = 1;
								tree = 0;
								continue;
							}
							if (distance(x,y,ix,iy) < 1) {
								skip = 1;
								tree = 0;
								continue;
							}
							e = unit_find_target(ix+(iy*mapgen.w),u->id);
				if (e && !player_is_ai(o->id))
				rtprintf(CN_DEBUG "4a: %d: %d %d",u->id,x,y);
							if (e && e->type == UNIT_VILLAGER && e->action == UACT_TEND) {
								skip = 1;
								tree = 0;
								continue;
							}
						}
						if (!tree) {
				if (!player_is_ai(o->id))
				rtprintf(CN_DEBUG "5: %d: %d %d",u->id,x,y);
							if (!player_is_ai(o->id))
								rtprintf(CN_DEBUG "no tree! %d: %d %d",u->id,x,y);
							if (!skip) {
								if (!player_is_ai(o->id))
									rtprintf(CN_DEBUG "has_trees = 0!: %d: %d %d",u->id,x,y);
								/*o->has_trees = 0;*/
							}
							while ((ix == x && iy == y) || mapgen_get_tile(ix,iy) == MAP_WATER || mapgen_get_tile(ix,iy) == MAP_BLANK) {
								ix = rand_range(x_c-16,x_c+16);
								iy = rand_range(y_c-16,y_c+16);
							}
						}else{
				if (!player_is_ai(o->id))
				rtprintf(CN_DEBUG "6: %d: %d %d",u->id,x,y);
							if (!player_is_ai(o->id))
								rtprintf(CN_DEBUG "tree: %d: %d %d -> %d %d",u->id,x,y,ix,iy);
						}
						u->step = -1;
						path_free(u->path);
						u->path = path_find(x,y,ix,iy);
						u->pos = x+(y*mapgen.w);
						t = array_get_ptr(u->path,1);
						u->target = t[0]+(t[1]*mapgen.w);
						u->action = UACT_MOVE;
					}
				}else{
				if (!player_is_ai(o->id))
				rtprintf(CN_DEBUG "7: %d: %d %d",u->id,x,y);
					ix = x;
					iy = y;
					while ((ix == x && iy == y) || mapgen_get_tile(ix,iy) == MAP_WATER || mapgen_get_tile(ix,iy) == MAP_BLANK) {
						ix = rand_range(x_c-16,x_c+16);
						iy = rand_range(y_c-16,y_c+16);
					}
					u->step = -1;
					path_free(u->path);
					u->path = path_find(x,y,ix,iy);
					u->pos = x+(y*mapgen.w);
					t = array_get_ptr(u->path,1);
					u->target = t[0]+(t[1]*mapgen.w);
					u->action = UACT_MOVE;
				}
			/* find somewhere a path should be */
			}else if (!o->has_paths) {
			/* find somewhere an outpost should be */
			}else if (!o->has_outposts) {
			/* find somewhere a wall should be */
			}else if (!o->has_walls) {
			}
		}else if (u->action == UACT_MOVE) {
			/* villager - cut down trees, or build a path/outpost/wall */
			/* got trees? cut 'em down */
			if (o->has_trees) {
				int x_c;
				int y_c;
				int centre = mapgen_find(o->id,MAP_VC1);
				x_c = centre%mapgen.w;
				y_c = centre/mapgen.w;
				if (!player_is_ai(o->id))
				rtprintf(CN_DEBUG "8: %d: %d %d",u->id,x,y);
				/* don't stray too far from the village */
				if (distance(x,y,x_c,y_c) < 18) {
					int tree = 0;
					int tx;
					int ty;
					x = u->target%mapgen.w;
					y = u->target/mapgen.w;
				if (!player_is_ai(o->id))
				rtprintf(CN_DEBUG "9: %d: %d %d",u->id,x,y);
					/* find a tree */
					for (ix=-1; !tree && ix<2; ix++) {
						for (iy=-1; !tree && iy<2; iy++) {
							if (!ix && !iy)
								continue;
							tree = mapgen_get_detail(x+ix,y+iy);
							if (
								tree == MAP_GRASSTREE1
								|| tree == MAP_GRASSTREE2
								|| tree == MAP_GRASSTREE3
								|| tree == MAP_GRASSTREE4
								|| tree == MAP_SANDTREE1
								|| tree == MAP_SANDTREE2
								|| tree == MAP_SANDTREE3
								|| tree == MAP_SANDTREE4
							) {
								e = unit_find_target((x+ix)+((y+iy)*mapgen.w),u->id);
				if (e && !player_is_ai(o->id))
				rtprintf(CN_DEBUG "9a: %d: %d %d",u->id,x,y);
								if (!e || e->type != UNIT_VILLAGER || e->action != UACT_TEND) {
									tx = x+ix;
									ty = y+iy;
									break;
								}
							}
							tree = 0;
						}
					}
					/* and cut it down */
					if (tree) {
				if (!player_is_ai(o->id))
				rtprintf(CN_DEBUG "10: %d: %d %d",u->id,x,y);
						u->action = UACT_TEND;
						u->step = -1;
						path_free(u->path);
						u->path = array_create(RTG_TYPE_PTR);
						t = malloc(sizeof(int)*2);
						t[0] = x;
						t[1] = y;
						array_push_ptr(u->path,t);
						t = malloc(sizeof(int)*2);
						t[0] = tx;
						t[1] = ty;
						array_push_ptr(u->path,t);
						u->pos = u->target;
						u->target = tx+(ty*mapgen.w);
					}
				}
			/* make a path */
			}else if (!o->has_paths) {
			/* build an outpost */
			}else if (!o->has_outposts) {
			/* build a wall */
			}else if (!o->has_walls) {
			}
		}else if (u->action == UACT_TEND) {
			/* remove tree */
			if (o->has_trees) {
				mapnode_t *mn = mapgen_get(u->target%mapgen.w,u->target/mapgen.w);
				mn->detail = MAP_BLANK;
				u->action = UACT_MOVE;
				u->step = -1;
				t = array_get_ptr(u->path,0);
				u->pos = t[0]+(t[1]*mapgen.w);
				t = array_get_ptr(u->path,1);
				u->target = t[0]+(t[1]*mapgen.w);
			/* make a path */
			}else if (!o->has_paths) {
				mapnode_t *mn = mapgen_get(u->target%mapgen.w,u->target/mapgen.w);
				mn->detail = MAP_PATH;
				u->action = UACT_MOVE;
				u->step = -1;
				t = array_get_ptr(u->path,0);
				u->pos = t[0]+(t[1]*mapgen.w);
				t = array_get_ptr(u->path,1);
				u->target = t[0]+(t[1]*mapgen.w);
			/* build an outpost */
			}else if (!o->has_outposts) {
			/* build a wall */
			}else if (!o->has_walls) {
			}
		}
	}
	if (u->action == UACT_TEND)
		o->credits++;
	x = u->pos%mapgen.w;
	y = u->pos/mapgen.w;
	if (u->action == UACT_MOVE) {
		t = array_get_ptr(u->path,u->step+2);
		if (t) {
			x = t[0];
			y = t[1];
		}else{
			t = array_get_ptr(u->path,u->step+1);
			if (t) {
				x = t[0];
				y = t[1];
			}else{
				x = u->target%mapgen.w;
				y = u->target/mapgen.w;
			}
		}
	}
	for (ix=-2; ix<3; ix++) {
		for (iy=-2; iy<3; iy++) {
			shade_set(u->owner,x+ix,y+iy,SHADE_BRIGHT);
		}
	}
}

void unit_step_farmer(unit_t *u, playerdata_t *o)
{
	unit_t *e;
	int *t;
	int x;
	int y;
	int i;
	int ix;
	int iy;
	/* check that attacking units are attacking an enemy */
	if (u->action == UACT_ATTACK) {
		e = unit_find_pos(u->target);
		if (!e || e->owner == u->owner) {
			u->action = UACT_MOVE;
		}else{
			/* TODO: combat */
		}
	}else{
		x = u->pos%mapgen.w;
		y = u->pos/mapgen.w;
		if (u->action == UACT_MOVE) {
			x = u->target%mapgen.w;
			y = u->target/mapgen.w;
		}
		e = NULL;
		i = 2;
		/* look for an enemy unit nearby */
		for (ix=-(i-1); !e && ix<i; ix++) {
			for (iy=-(i-1); !e && iy<i; iy++) {
				if (!ix && !iy)
					continue;
				e = unit_find_pos((x+ix)+((y+iy)*mapgen.w));
				if (e && e->owner == u->owner)
					e = NULL;
			}
		}
		/* attack if found */
		if (e) {
			path_free(u->path);
			u->path = path_find(x,y,x+ix,y+iy);
			if (u->path->length > 2) {
				u->step = -1;
				u->action = UACT_MOVE;
				t = array_get_ptr(u->path,0);
				u->pos = t[0]+(t[1]*mapgen.w);
				t = array_get_ptr(u->path,1);
				u->target = t[0]+(t[1]*mapgen.w);
			}else{
				u->action = UACT_ATTACK;
				u->target = (x+ix)+((y+iy)*mapgen.w);
			}
		/* if the unit is standing around, find them something to do */
		}else if (u->action == UACT_STAND) {
			/* farmers - find a farm, go to it */
			u->target = unit_find_farm(u->owner);
			path_free(u->path);
			u->path = path_find(x,y,u->target%mapgen.w,u->target/mapgen.w);
			if (u->pos != u->target) {
				u->action = UACT_MOVE;
			}else{
				u->action = UACT_TEND;
			}
			u->step = -1;
		}else if (u->action == UACT_MOVE) {
			/* if farmers are in front of a farm, start tending to it */
			if (u->step == u->path->length-3) {
				t = array_get_ptr(u->path,u->path->length-1);
				u->pos = u->target;
				u->target = t[0]+(t[1]*mapgen.w);
				path_free(u->path);
				u->path = path_find(u->pos%mapgen.w,u->pos/mapgen.w,u->target%mapgen.w,u->target/mapgen.w);
				u->action = UACT_TEND;
				u->step = -1;
			}
		}
	}
	if (u->action == UACT_TEND) {
		o->credits += 2;
	}
	x = u->pos%mapgen.w;
	y = u->pos/mapgen.w;
	if (u->action == UACT_MOVE) {
		t = array_get_ptr(u->path,u->step+2);
		if (t) {
			x = t[0];
			y = t[1];
		}else{
			t = array_get_ptr(u->path,u->step+1);
			if (t) {
				x = t[0];
				y = t[1];
			}else{
				x = u->target%mapgen.w;
				y = u->target/mapgen.w;
			}
		}
	}
	for (ix=-2; ix<3; ix++) {
		for (iy=-2; iy<3; iy++) {
			shade_set(u->owner,x+ix,y+iy,SHADE_BRIGHT);
		}
	}
}

void unit_step_master(unit_t *u, playerdata_t *o)
{
	unit_t *e;
	int *t;
	int x;
	int y;
	int i;
	int ix;
	int iy;
	/* check that attacking units are attacking an enemy */
	if (u->action == UACT_ATTACK) {
		e = unit_find_pos(u->target);
		if (!e || e->owner == u->owner) {
			u->action = UACT_MOVE;
		}else{
			/* TODO: combat */
		}
	}else{
		x = u->pos%mapgen.w;
		y = u->pos/mapgen.w;
		if (u->action == UACT_MOVE) {
			x = u->target%mapgen.w;
			y = u->target/mapgen.w;
		}
		e = NULL;
		i = 2;
		/* look for an enemy unit nearby */
		for (ix=-(i-1); !e && ix<i; ix++) {
			for (iy=-(i-1); !e && iy<i; iy++) {
				if (!ix && !iy)
					continue;
				e = unit_find_pos((x+ix)+((y+iy)*mapgen.w));
				if (e && e->owner == u->owner)
					e = NULL;
			}
		}
		/* attack if found */
		if (e) {
			path_free(u->path);
			u->path = path_find(x,y,x+ix,y+iy);
			if (u->path->length > 2) {
				u->step = -1;
				u->action = UACT_MOVE;
				t = array_get_ptr(u->path,0);
				u->pos = t[0]+(t[1]*mapgen.w);
				t = array_get_ptr(u->path,1);
				u->target = t[0]+(t[1]*mapgen.w);
			}else{
				u->action = UACT_ATTACK;
				u->target = (x+ix)+((y+iy)*mapgen.w);
			}
		/* if the unit is standing around, find them something to do */
		}else if (u->action == UACT_STAND) {
			/* farmers - find a farm, go to it */
			u->target = unit_find_farm(u->owner);
			path_free(u->path);
			u->path = path_find(x,y,u->target%mapgen.w,u->target/mapgen.w);
			if (u->pos != u->target) {
				u->action = UACT_MOVE;
			}else{
				u->action = UACT_TEND;
			}
			u->step = -1;
		}else if (u->action == UACT_MOVE) {
			/* if farmers are in front of a farm, start tending to it */
			if (u->step == u->path->length-3) {
				t = array_get_ptr(u->path,u->path->length-1);
				u->pos = u->target;
				u->target = t[0]+(t[1]*mapgen.w);
				path_free(u->path);
				u->path = path_find(u->pos%mapgen.w,u->pos/mapgen.w,u->target%mapgen.w,u->target/mapgen.w);
				u->action = UACT_TEND;
				u->step = -1;
			}
		}
	}
	if (u->action == UACT_TEND) {
		o->credits += 2;
	}
	x = u->pos%mapgen.w;
	y = u->pos/mapgen.w;
	if (u->action == UACT_MOVE) {
		t = array_get_ptr(u->path,u->step+2);
		if (t) {
			x = t[0];
			y = t[1];
		}else{
			t = array_get_ptr(u->path,u->step+1);
			if (t) {
				x = t[0];
				y = t[1];
			}else{
				x = u->target%mapgen.w;
				y = u->target/mapgen.w;
			}
		}
	}
	for (ix=-2; ix<3; ix++) {
		for (iy=-2; iy<3; iy++) {
			shade_set(u->owner,x+ix,y+iy,SHADE_BRIGHT);
		}
	}
}

void unit_step_soldier(unit_t *u, playerdata_t *o)
{
	unit_t *e;
	int *t;
	int x;
	int y;
	int i;
	int ix;
	int iy;
	/* check that attacking units are attacking an enemy */
	if (u->action == UACT_ATTACK) {
		e = unit_find_pos(u->target);
		if (!e || e->owner == u->owner) {
			u->action = UACT_MOVE;
		}else{
			/* TODO: combat */
		}
	}else{
		x = u->pos%mapgen.w;
		y = u->pos/mapgen.w;
		if (u->action == UACT_MOVE) {
			x = u->target%mapgen.w;
			y = u->target/mapgen.w;
		}
		e = NULL;
		i = 4;
		/* look for an enemy unit nearby */
		for (ix=-(i-1); !e && ix<i; ix++) {
			for (iy=-(i-1); !e && iy<i; iy++) {
				if (!ix && !iy)
					continue;
				e = unit_find_pos((x+ix)+((y+iy)*mapgen.w));
				if (e && e->owner == u->owner)
					e = NULL;
			}
		}
		/* attack if found */
		if (e) {
			path_free(u->path);
			u->path = path_find(x,y,x+ix,y+iy);
			if (u->path->length > 2) {
				u->step = -1;
				u->action = UACT_MOVE;
				t = array_get_ptr(u->path,0);
				u->pos = t[0]+(t[1]*mapgen.w);
				t = array_get_ptr(u->path,1);
				u->target = t[0]+(t[1]*mapgen.w);
			}else{
				u->action = UACT_ATTACK;
				u->target = (x+ix)+((y+iy)*mapgen.w);
			}
		/* if the unit is standing around, find them something to do */
		}else if (u->action == UACT_STAND) {
			/* soldiers - go to barracks */
			u->target = mapgen_find(u->owner,MAP_BARRACKS1);
			path_free(u->path);
			u->path = path_find(x,y,u->target%mapgen.w,u->target/mapgen.w);
			if (u->pos != u->target)
				u->action = UACT_MOVE;
			u->step = -1;
		}
	}
	x = u->pos%mapgen.w;
	y = u->pos/mapgen.w;
	if (u->action == UACT_MOVE) {
		t = array_get_ptr(u->path,u->step+2);
		if (t) {
			x = t[0];
			y = t[1];
		}else{
			t = array_get_ptr(u->path,u->step+1);
			if (t) {
				x = t[0];
				y = t[1];
			}else{
				x = u->target%mapgen.w;
				y = u->target/mapgen.w;
			}
		}
	}
	for (ix=-2; ix<3; ix++) {
		for (iy=-2; iy<3; iy++) {
			shade_set(u->owner,x+ix,y+iy,SHADE_BRIGHT);
		}
	}
}

void unit_step_brigadier(unit_t *u, playerdata_t *o)
{
	unit_t *e;
	int *t;
	int x;
	int y;
	int i;
	int ix;
	int iy;
	/* check that attacking units are attacking an enemy */
	if (u->action == UACT_ATTACK) {
		e = unit_find_pos(u->target);
		if (!e || e->owner == u->owner) {
			u->action = UACT_MOVE;
		}else{
			/* TODO: combat */
		}
	}else{
		x = u->pos%mapgen.w;
		y = u->pos/mapgen.w;
		if (u->action == UACT_MOVE) {
			x = u->target%mapgen.w;
			y = u->target/mapgen.w;
		}
		e = NULL;
		i = 4;
		/* look for an enemy unit nearby */
		for (ix=-(i-1); !e && ix<i; ix++) {
			for (iy=-(i-1); !e && iy<i; iy++) {
				if (!ix && !iy)
					continue;
				e = unit_find_pos((x+ix)+((y+iy)*mapgen.w));
				if (e && e->owner == u->owner)
					e = NULL;
			}
		}
		/* attack if found */
		if (e) {
			path_free(u->path);
			u->path = path_find(x,y,x+ix,y+iy);
			if (u->path->length > 2) {
				u->step = -1;
				u->action = UACT_MOVE;
				t = array_get_ptr(u->path,0);
				u->pos = t[0]+(t[1]*mapgen.w);
				t = array_get_ptr(u->path,1);
				u->target = t[0]+(t[1]*mapgen.w);
			}else{
				u->action = UACT_ATTACK;
				u->target = (x+ix)+((y+iy)*mapgen.w);
			}
		/* if the unit is standing around, find them something to do */
		}else if (u->action == UACT_STAND) {
			/* soldiers - go to barracks */
			u->target = mapgen_find(u->owner,MAP_BARRACKS1);
			path_free(u->path);
			u->path = path_find(x,y,u->target%mapgen.w,u->target/mapgen.w);
			if (u->pos != u->target)
				u->action = UACT_MOVE;
			u->step = -1;
		}
	}
	x = u->pos%mapgen.w;
	y = u->pos/mapgen.w;
	if (u->action == UACT_MOVE) {
		t = array_get_ptr(u->path,u->step+2);
		if (t) {
			x = t[0];
			y = t[1];
		}else{
			t = array_get_ptr(u->path,u->step+1);
			if (t) {
				x = t[0];
				y = t[1];
			}else{
				x = u->target%mapgen.w;
				y = u->target/mapgen.w;
			}
		}
	}
	for (ix=-2; ix<3; ix++) {
		for (iy=-2; iy<3; iy++) {
			shade_set(u->owner,x+ix,y+iy,SHADE_BRIGHT);
		}
	}
}

void unit_step_scout(unit_t *u, playerdata_t *o)
{
	unit_t *e;
	int *t;
	int x;
	int y;
	int i;
	int ix;
	int iy;
	/* check that attacking units are attacking an enemy */
	if (u->action == UACT_ATTACK) {
		e = unit_find_pos(u->target);
		if (!e || e->owner == u->owner) {
			u->action = UACT_MOVE;
		}else{
			/* TODO: combat */
		}
	}else{
		x = u->pos%mapgen.w;
		y = u->pos/mapgen.w;
		if (u->action == UACT_MOVE) {
			x = u->target%mapgen.w;
			y = u->target/mapgen.w;
		}
		e = NULL;
		i = 3;
		/* look for an enemy unit nearby */
		for (ix=-(i-1); !e && ix<i; ix++) {
			for (iy=-(i-1); !e && iy<i; iy++) {
				if (!ix && !iy)
					continue;
				e = unit_find_pos((x+ix)+((y+iy)*mapgen.w));
				if (e && e->owner == u->owner)
					e = NULL;
			}
		}
		/* attack if found */
		if (e) {
			path_free(u->path);
			u->path = path_find(x,y,x+ix,y+iy);
			if (u->path->length > 2) {
				u->step = -1;
				u->action = UACT_MOVE;
				t = array_get_ptr(u->path,0);
				u->pos = t[0]+(t[1]*mapgen.w);
				t = array_get_ptr(u->path,1);
				u->target = t[0]+(t[1]*mapgen.w);
			}else{
				u->action = UACT_ATTACK;
				u->target = (x+ix)+((y+iy)*mapgen.w);
			}
		/* if the unit is standing around, find them something to do */
		}else{
			/* if not found, search wider */
			e = NULL;
			i = 6;
			/* look for an enemy unit nearby */
			for (ix=-(i-1); !e && ix<i; ix++) {
				for (iy=-(i-1); !e && iy<i; iy++) {
					if (!ix && !iy)
						continue;
					e = unit_find_pos((x+ix)+((y+iy)*mapgen.w));
					if (e && e->owner == u->owner)
						e = NULL;
				}
			}
			/* report if found */
			if (e) {
			}else if (u->action == UACT_STAND) {
				/* scout - wander the map */
				ix = x;
				iy = y;
				while ((ix == x && iy == y) || mapgen_get_tile(ix,iy) == MAP_WATER || mapgen_get_tile(ix,iy) == MAP_BLANK) {
					ix = rand_range(2,mapgen.w-2);
					iy = rand_range(2,mapgen.h-2);
				}
				u->step = -1;
				path_free(u->path);
				u->path = path_find(x,y,ix,iy);
				u->pos = x+(y*mapgen.w);
				t = array_get_ptr(u->path,1);
				u->target = t[0]+(t[1]*mapgen.w);
				u->action = UACT_MOVE;
			}
		}
	}
	x = u->pos%mapgen.w;
	y = u->pos/mapgen.w;
	if (u->action == UACT_MOVE) {
		t = array_get_ptr(u->path,u->step+2);
		if (t) {
			x = t[0];
			y = t[1];
		}else{
			t = array_get_ptr(u->path,u->step+1);
			if (t) {
				x = t[0];
				y = t[1];
			}else{
				x = u->target%mapgen.w;
				y = u->target/mapgen.w;
			}
		}
	}
	for (ix=-4; ix<5; ix++) {
		for (iy=-4; iy<5; iy++) {
			shade_set(u->owner,x+ix,y+iy,SHADE_BRIGHT);
		}
	}
}

typedef void (*stepfn)(unit_t *, playerdata_t *);
static stepfn unit_step_types[6] = {
	unit_step_villager,
	unit_step_farmer,
	unit_step_master,
	unit_step_soldier,
	unit_step_brigadier,
	unit_step_scout
};

/* advance units by one step */
void unit_step()
{
	unit_t *u = units;
	unit_t *e;
	array_t *p;
	playerdata_t *o;
	int i;
	int *k;
	int *t;

	/* first loop, check what the unit is doing, and reassign if necessary */
	while (u) {
		/* check for the unit's owner, if none of defeated, kill the unit */
		o = game_get_playerdata(u->owner);
		if (u->hp < 1 || !o || o->defeated) {
			e = u;
			u = u->next;
			player_unit(-1,e->owner,e->type,e->id,0,UACT_STAND,e->path);
			units = list_remove(&units,e);
			continue;
		}
		unit_step_types[u->type](u,o);
		u = u->next;
	}

	u = units;
	/* second loop, move the unit, and update the player about what the unit's doing */
	while (u) {
		u->step++;
		p = array_create(RTG_TYPE_PTR);

		if (u->action == UACT_MOVE) {
			for (i=u->step; i<u->path->length; i++) {
				k = array_get_ptr(u->path,i);
				array_push_ptr(p,k);
			}
			if (!p->length) {
				k = array_get_ptr(u->path,u->path->length-1);
				array_push_ptr(p,k);
				u->action = UACT_STAND;
				u->step = 0;
			}
		}else if (u->action == UACT_ATTACK) {
			u->step = 0;
			for (i=0; i<u->path->length; i++) {
				k = array_get_ptr(u->path,i);
				array_push_ptr(p,k);
			}
		}else if (u->action == UACT_TEND) {
			u->step = 0;
			for (i=0; i<u->path->length; i++) {
				k = array_get_ptr(u->path,i);
				array_push_ptr(p,k);
			}
		}else{
			u->action = UACT_STAND;
			u->step = 0;
			for (i=0; i<u->path->length; i++) {
				k = array_get_ptr(u->path,i);
				array_push_ptr(p,k);
			}
		}
		if (p->length > 1) {
			t = array_get_ptr(p,0);
			u->pos = t[0]+(t[1]*mapgen.w);
			t = array_get_ptr(p,1);
			u->target = t[0]+(t[1]*mapgen.w);
		}else{
			t = array_get_ptr(p,0);
			u->pos = t[0]+(t[1]*mapgen.w);
			u->target = u->pos;
		}
		player_unit(-1,u->owner,u->type,u->id,u->hp,u->action,p);
		array_free(p);
		u = u->next;
	}
}

unit_t *unit_find_villager(int id)
{
	unit_t *u = units;
	while (u) {
		if (u->owner == id && u->type == UNIT_VILLAGER && u->action != UACT_TEND)
			return u;
		u = u->next;
	}
	u = units;
	while (u) {
		if (u->owner == id && u->type == UNIT_VILLAGER)
			return u;
		u = u->next;
	}
	return NULL;
}

unit_t *unit_find_pos(int pos)
{
	unit_t *u = units;
	while (u) {
		if (u->pos == pos)
			return u;
		u = u->next;
	}
	return NULL;
}

unit_t *unit_find_target(int pos, int except)
{
	unit_t *u = units;
	while (u) {
		if (u->id != except && u->target == pos)
			return u;
		u = u->next;
	}
	return NULL;
}

unit_t *unit_find_path(int pos)
{
	int i;
	unit_t *u = units;
	while (u) {
		if (u->pos == pos)
			return u;
		if (u->path) {
			for (i=0; i<u->path->length; i++) {
				if (array_get_int(u->path,i) == pos)
					return u;
			}
		}
		u = u->next;
	}
	return NULL;
}
