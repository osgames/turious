/************************************************************************
* client.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

#define CLIENT_INACTIVE	0
#define CLIENT_LOCAL	1
#define CLIENT_REMOTE	2

client_t client = {
	CLIENT_INACTIVE,
	NULL,
	NULL,
	NULL,
	{0,0},
	0
};

static void client_send_action(action_t *a)
{
	/* TODO: networkiness */
	free(a);
}

static int client_error()
{
	rtg_state = RTG_STATE_MENU;
	cmd_execf("exec ui.elements(*.!.*.style.visible) false");
	cmd_execf("exec ui.elements(entry.!.*.style.visible) true");
	return EVENT_HANDLED;
}

static void client_unit(int id, int owner, int unit, int uid, int hp, int action, array_t *path)
{
	unit_info_t *u;

	if (!path && hp)
		return;

	u = malloc(sizeof(unit_info_t));
	if (!u)
		return;
	u->id = uid;
	u->owner = owner;
	u->type = unit;
	u->hp = hp;
	u->action = action;
	u->path = NULL;
	if (path)
		u->path = path_copy(path);

	mutex_lock(client.mut);
	client.cunits = list_push(&client.cunits,u);
	mutex_unlock(client.mut);
}

static void client_data(int id, int c, int s, int u[UNIT_COUNT])
{
	ui_widget_set_value("game.label.villagers","%d",u[UNIT_VILLAGER]);
	ui_widget_set_value("game.label.farmers","%d",u[UNIT_FARMER]);
	ui_widget_set_value("game.label.masters","%d",u[UNIT_MASTER]);
	ui_widget_set_value("game.label.soldiers","%d",u[UNIT_SOLDIER]);
	ui_widget_set_value("game.label.brigadiers","%d",u[UNIT_BRIGADIER]);
	ui_widget_set_value("game.label.scouts","%d",u[UNIT_SCOUT]);

	ui_widget_set_value("town.label.vg-current","You have: %d",u[UNIT_VILLAGER]);
	ui_widget_set_value("farm.label.fm-current","You have: %d",u[UNIT_FARMER]);
	ui_widget_set_value("farm.label.mf-current","You have: %d",u[UNIT_MASTER]);
	ui_widget_set_value("barracks.label.sd-current","You have: %d",u[UNIT_SOLDIER]);
	ui_widget_set_value("barracks.label.bg-current","You have: %d",u[UNIT_BRIGADIER]);
	ui_widget_set_value("barracks.label.st-current","You have: %d",u[UNIT_SCOUT]);

	ui_widget_set_value("game.label.credits","%d",c);
	ui_widget_set_value("game.label.strength","%d",s);
}

static void client_msg(int id, char* str)
{
	if (str[0] == '\r') {
		texteffect_add(str+1);
	}else{
		rtprintf(CN_NOTICE "%s",str);
	}
}

static void client_sig(int id, char* str)
{
	sound_play_effect("scout");
	rtprintf(CN_NOTICE "%s",str);
}

static void client_map(int id, mapnode_t *data, int w, int h)
{
	int l;
	mutex_lock(client.mut);
	if (client.cmap)
		free(client.cmap);

	client.cmaps[0] = w;
	client.cmaps[1] = h;

	l = w*h*4;

	client.cmap = malloc(l);
	memcpy(client.cmap,data,l);
	mutex_unlock(client.mut);
}

static void client_state(int id, int state)
{
	if (client.mode == CLIENT_LOCAL) {
		switch (state) {
		case PLAYER_WON:
			ui_msg(UIMSG_OK,"You won!",NULL);
		case PLAYER_END:
			ui_style("*.!.*","visible","false");
			ui_style("entry.!.*","visible","true");
			rtg_state = RTG_STATE_MENU;
			client_disconnect();
			break;
		case PLAYER_DEFEATED:
			ui_msg(UIMSG_OK,"You were defeated",NULL);
			break;
		default:;
		}
	}else{
		switch (state) {
		case PLAYER_WON:
			ui_msg(UIMSG_OK,"You won!",NULL);
		case PLAYER_END:
			break;
		case PLAYER_DEFEATED:
			ui_msg(UIMSG_OK,"You were defeated",NULL);
			break;
		default:;
		}
	}
}

static void client_init(int id, int vg_c, int fm_c, int mf_c, int sd_c, int bg_c, int sc_c, int ug_c, array_t *players)
{
	client.cid = id;
	ui_widget_set_value("town.label.vg-cost","Villagers cost %d credits to train",vg_c);
	ui_widget_set_value("farm.label.fm-cost","Farmers cost %d credits to train",fm_c);
	ui_widget_set_value("farm.label.mf-cost","Master Farmers cost %d credits to train",mf_c);
	ui_widget_set_value("barracks.label.sd-cost","Soldiers cost %d credits to train",sd_c);
	ui_widget_set_value("barracks.label.bg-cost","Brigadiers cost %d credits to train",bg_c);
	ui_widget_set_value("barracks.label.st-cost","Scouts cost %d credits to train",sc_c);

	if (players) {
		int i;
		char* p;
		for (i=0; i<players->length; i++) {
			p = array_get_string(players,i);
			rtprintf(CN_DEBUG "%d: '%s'",i,p);
		}
	}
}

void client_connect_local()
{
	char* p;
	comset_t *c = malloc(sizeof(comset_t));
	c->type = CLIENT_HUMAN;
	c->unit = client_unit;
	c->data = client_data;
	c->msg = client_msg;
	c->sig = client_sig;
	c->map = client_map;
	c->state = client_state;
	c->init = client_init;

	p = config_get("game.player_name");
	if (!p)
		p = "Local Player";

	if (!client.mut)
		client.mut = mutex_create();

	if ((client.cid = player_connect(p,c)) < 1 || game_init()) {
		rtprintf(CN_ERROR "could not initialise game");
		ui_msg(UIMSG_OK,"Error: Could not initialise game.",client_error);
		free(c);
		return;
	}
	client.mode = CLIENT_LOCAL;
}

void client_connect_remote(char* server, char* port)
{
	client.mode = CLIENT_REMOTE;
	/* TODO: networkiness */
}

void client_disconnect()
{
	int cm = client.mode;
	client.mode = CLIENT_INACTIVE;

	switch (cm) {
	case CLIENT_LOCAL:
		player_disconnect(client.cid);
		break;
	case CLIENT_REMOTE:
		/* TODO: networkiness */
		break;
	default:
		return;
	}
	map_unload();
}

int client_is_local()
{
	if (client.mode == CLIENT_LOCAL)
		return 1;

	return 0;
}

void client_main()
{
	if (client.mode == CLIENT_INACTIVE)
		return;

	mutex_lock(client.mut);
	if (client.cmap) {
		map_load(client.cmap,client.cmaps[0],client.cmaps[1]);
		free(client.cmap);
		client.cmap = NULL;
	}
	if (client.cunits) {
		village_unit_t *u;
		unit_info_t *cu;
		array_t *p;
		int *start;
		int *end;
		int *face;
		int bx;
		int by;
		int ex;
		int ey;
		int d;

		while ((cu = list_pull(&client.cunits))) {

			if (cu->path) {
				start = array_get_ptr(cu->path,0);
				if (cu->path->length > 1) {
					end = array_get_ptr(cu->path,1);
					if (cu->action == UACT_MOVE) {
						face = end;
					}else{
						face = end;
						end = start;
					}
				}else{
					end = start;
					face = start;
				}

				village_to_screen(start[0],start[1],&bx,&by);
				village_to_screen(end[0],end[1],&ex,&ey);

				d = direction(start[0],start[1],face[0],face[1]);
			}

			u = village_unit(cu->id);
			if (!u) {
				if (!cu->hp) {
					free(cu);
					continue;
				}
				u = malloc(sizeof(village_unit_t));
				if (!u) {
					free(cu);
					continue;
				}
				u->id = cu->id;
				u->owner = cu->owner;
				u->type = cu->type;
				u->hp = cu->hp;
				u->action = cu->action;
				u->reset = 0;
				u->path = cu->path;
				u->pos[0] = start[0];
				u->pos[1] = start[1];
				u->target[0] = end[0];
				u->target[1] = end[1];
				u->anim = anim_spawn((cu->type|cu->action),d,-1,bx,by,ex,ey);
				u->anim->u = 1;
				village.units = list_push(&village.units,u);
			}else if (!cu->hp) {
				village.units = list_remove(&village.units,u);
				path_free(u->path);
				free(u);
			}else{
				u->hp = cu->hp;
				u->action = cu->action;
				u->reset = 0;
				p = u->path;
				u->path = cu->path;
				path_free(p);
				u->pos[0] = start[0];
				u->pos[1] = start[1];
				u->target[0] = end[0];
				u->target[1] = end[1];
				u->anim->loop = 0;
				u->anim->u = 0;
				u->anim = anim_spawn((cu->type|cu->action),d,-1,bx,by,ex,ey);
			}
			free(cu);
		}
	}
	mutex_unlock(client.mut);
}

int client_train_villager(widget_t *w)
{
	action_t *a;

rtprintf(CN_DEBUG "client_train_villager");

	a = malloc(sizeof(action_t));
	a->action = ACTION_TRAIN;
	a->units[UNIT_VILLAGER] = 1;
	a->units[UNIT_FARMER] = 0;
	a->units[UNIT_MASTER] = 0;
	a->units[UNIT_SOLDIER] = 0;
	a->units[UNIT_BRIGADIER] = 0;
	a->units[UNIT_SCOUT] = 0;

	a->owner = client.cid;

	if (client.mode == CLIENT_LOCAL) {
		int e = game_push_action(a);
		if (e) {
			client_msg(a->owner,acterrors[e]);
			free(a);
		}
	}else{
		client_send_action(a);
	}

	return EVENT_HANDLED;
}

int client_train_farmer(widget_t *w)
{
	action_t *a;

	a = malloc(sizeof(action_t));
	a->action = ACTION_TRAIN;
	a->units[UNIT_VILLAGER] = 0;
	a->units[UNIT_FARMER] = 1;
	a->units[UNIT_MASTER] = 0;
	a->units[UNIT_SOLDIER] = 0;
	a->units[UNIT_BRIGADIER] = 0;
	a->units[UNIT_SCOUT] = 0;

	a->owner = client.cid;

	if (client.mode == CLIENT_LOCAL) {
		int e = game_push_action(a);
		if (e) {
			client_msg(a->owner,acterrors[e]);
			free(a);
		}
	}else{
		client_send_action(a);
	}

	return EVENT_HANDLED;
}

int client_train_master(widget_t *w)
{
	action_t *a;

	a = malloc(sizeof(action_t));
	a->action = ACTION_TRAIN;
	a->units[UNIT_VILLAGER] = 0;
	a->units[UNIT_FARMER] = 0;
	a->units[UNIT_MASTER] = 1;
	a->units[UNIT_SOLDIER] = 0;
	a->units[UNIT_BRIGADIER] = 0;
	a->units[UNIT_SCOUT] = 0;

	a->owner = client.cid;

	if (client.mode == CLIENT_LOCAL) {
		int e = game_push_action(a);
		if (e) {
			client_msg(a->owner,acterrors[e]);
			free(a);
		}
	}else{
		client_send_action(a);
	}

	return EVENT_HANDLED;
}

int client_train_soldier(widget_t *w)
{
	action_t *a;

	a = malloc(sizeof(action_t));
	a->action = ACTION_TRAIN;
	a->units[UNIT_VILLAGER] = 0;
	a->units[UNIT_FARMER] = 0;
	a->units[UNIT_MASTER] = 0;
	a->units[UNIT_SOLDIER] = 1;
	a->units[UNIT_BRIGADIER] = 0;
	a->units[UNIT_SCOUT] = 0;

	a->owner = client.cid;

	if (client.mode == CLIENT_LOCAL) {
		int e = game_push_action(a);
		if (e) {
			client_msg(a->owner,acterrors[e]);
			free(a);
		}
	}else{
		client_send_action(a);
	}

	return EVENT_HANDLED;
}

int client_train_brigadier(widget_t *w)
{
	action_t *a;

	a = malloc(sizeof(action_t));
	a->action = ACTION_TRAIN;
	a->units[UNIT_VILLAGER] = 0;
	a->units[UNIT_FARMER] = 0;
	a->units[UNIT_MASTER] = 0;
	a->units[UNIT_SOLDIER] = 0;
	a->units[UNIT_BRIGADIER] = 1;
	a->units[UNIT_SCOUT] = 0;

	a->owner = client.cid;

	if (client.mode == CLIENT_LOCAL) {
		int e = game_push_action(a);
		if (e) {
			client_msg(a->owner,acterrors[e]);
			free(a);
		}
	}else{
		client_send_action(a);
	}

	return EVENT_HANDLED;
}

int client_train_scout(widget_t *w)
{
	action_t *a;

	a = malloc(sizeof(action_t));
	a->action = ACTION_TRAIN;
	a->units[UNIT_VILLAGER] = 0;
	a->units[UNIT_FARMER] = 0;
	a->units[UNIT_MASTER] = 0;
	a->units[UNIT_SOLDIER] = 0;
	a->units[UNIT_BRIGADIER] = 0;
	a->units[UNIT_SCOUT] = 1;

	a->owner = client.cid;

	if (client.mode == CLIENT_LOCAL) {
		int e = game_push_action(a);
		if (e) {
			client_msg(a->owner,acterrors[e]);
			free(a);
		}
	}else{
		client_send_action(a);
	}

	return EVENT_HANDLED;
}

int client_attack(widget_t *w)
{
	int t;
	int s;
	int b;
	char* v;
	action_t *a;

	v = ui_widget_get_value("attack.num.player");
	if (!v)
		return EVENT_HANDLED;

	t = strtol(v,NULL,10);
	if (!t)
		return EVENT_HANDLED;

	v = ui_widget_get_value("attack.num.soldiers");
	if (!v)
		return EVENT_HANDLED;

	s = strtol(v,NULL,10);
	if (!s)
		return EVENT_HANDLED;

	v = ui_widget_get_value("attack.num.brigadiers");
	if (!v)
		return EVENT_HANDLED;

	b = strtol(v,NULL,10);

	ui_style("attack.!.*","visible","false");

	a = malloc(sizeof(action_t));
	a->action = ACTION_ATTACK;
	a->units[UNIT_VILLAGER] = 0;
	a->units[UNIT_FARMER] = 0;
	a->units[UNIT_MASTER] = 0;
	a->units[UNIT_SOLDIER] = s;
	a->units[UNIT_BRIGADIER] = b;
	a->units[UNIT_SCOUT] = 0;

	a->owner = client.cid;
	a->target = t;

	if (client.mode == CLIENT_LOCAL) {
		int e = game_push_action(a);
		if (e) {
			client_msg(a->owner,acterrors[e]);
			free(a);
		}
	}else{
		client_send_action(a);
	}

	return EVENT_HANDLED;
}
