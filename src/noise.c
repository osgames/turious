/************************************************************************
* noise.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

static int smul;
int seed;

void noise_seed(int s)
{
	seed = s;
	smul = seed*seed;
}

double noise2d(int x, int y)
{
	int n = (x * x + y * y + smul) & 0x7fffffff;
	n = (n>>13)^n;
	n = (n * (n*n*60493+19990303) + 1376312589) & 0x7fffffff;
	return 1.0 - (double)n/1073741824.0;
}

unsigned int noise_base(int x, int y)
{
	double r = 0;
	double i;
	register int xi;
	register int yi;
	int xm = x+10;
	int ym = y+10;
	int ymul;
	int n;
	unsigned int d;

	for (yi=y-10; yi<ym; yi++) {
		ymul = yi*yi;
		for (xi=x-10; xi<xm; xi++) {
			n = (xi * xi + ymul + smul) & 0x7fffffff;
			n = (n>>13)^n;
			n = (n * (n*n*60493+19990303) + 1376312589) & 0x7fffffff;
			i = 1.0 - (double)n/1073741824.0;
			if (i<0.0)
				i *= -1.0;
			r += i;
		}
	}

	r /= 200.0;

	d = (unsigned int)(r*20.0);

	if (d<0)
		d *= -1;

	return d;
}

unsigned char noise(int x, int y)
{
	unsigned int d = noise_base(x,y);
	if (d>7)
		return d%8;

	return d;
}

unsigned char noise_height(int x, int y)
{
	unsigned int d = noise_base(x,y);
	if (d>255)
		return d%256;

	return d;
}
