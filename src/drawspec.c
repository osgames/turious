/************************************************************************
* drawspec.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2014 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

static drawspec_t drawspecs[256];
static int drawspec_isinit = 0;

void drawspec_init()
{
	drawspecs[MAP_BLANK].c.r = 0;
	drawspecs[MAP_BLANK].c.g = 0;
	drawspecs[MAP_BLANK].c.b = 0;
	drawspecs[MAP_BLANK].c.a = 0;
	drawspecs[MAP_BLANK].texture = -1;
	drawspecs[MAP_BLANK].sprite = -1;
	drawspecs[MAP_BLANK].click = 0;

	drawspecs[MAP_GRASS].c.r = 40;
	drawspecs[MAP_GRASS].c.g = 150;
	drawspecs[MAP_GRASS].c.b = 50;
	drawspecs[MAP_GRASS].c.a = 200;
	drawspecs[MAP_GRASS].texture = 0;
	drawspecs[MAP_GRASS].sprite = 0;
	drawspecs[MAP_GRASS].click = 0;

	drawspecs[MAP_SAND].c.r = 180;
	drawspecs[MAP_SAND].c.g = 180;
	drawspecs[MAP_SAND].c.b = 100;
	drawspecs[MAP_SAND].c.a = 200;
	drawspecs[MAP_SAND].texture = 0;
	drawspecs[MAP_SAND].sprite = 16;
	drawspecs[MAP_SAND].click = 0;

	drawspecs[MAP_WATER].c.r = 40;
	drawspecs[MAP_WATER].c.g = 60;
	drawspecs[MAP_WATER].c.b = 150;
	drawspecs[MAP_WATER].c.a = 200;
	drawspecs[MAP_WATER].texture = 0;
	drawspecs[MAP_WATER].sprite = 32;
	drawspecs[MAP_WATER].click = 0;

	drawspecs[MAP_PATH].c.r = 40;
	drawspecs[MAP_PATH].c.g = 150;
	drawspecs[MAP_PATH].c.b = 80;
	drawspecs[MAP_PATH].c.a = 200;
	drawspecs[MAP_PATH].texture = 0;
	drawspecs[MAP_PATH].sprite = 67;
	drawspecs[MAP_PATH].click = 0;

	drawspecs[MAP_VC1].c.r = 255;
	drawspecs[MAP_VC1].c.g = 0;
	drawspecs[MAP_VC1].c.b = 0;
	drawspecs[MAP_VC1].c.a = 200;
	drawspecs[MAP_VC1].texture = 3;
	drawspecs[MAP_VC1].sprite = 1;
	drawspecs[MAP_VC1].click = 0;

	drawspecs[MAP_VC2].c.r = 255;
	drawspecs[MAP_VC2].c.g = 0;
	drawspecs[MAP_VC2].c.b = 0;
	drawspecs[MAP_VC2].c.a = 200;
	drawspecs[MAP_VC2].texture = -1;
	drawspecs[MAP_VC2].sprite = -1;
	drawspecs[MAP_VC2].click = 0;

	drawspecs[MAP_VC1|MAP_OWN].c.r = 255;
	drawspecs[MAP_VC1|MAP_OWN].c.g = 255;
	drawspecs[MAP_VC1|MAP_OWN].c.b = 255;
	drawspecs[MAP_VC1|MAP_OWN].c.a = 200;
	drawspecs[MAP_VC1|MAP_OWN].texture = 3;
	drawspecs[MAP_VC1|MAP_OWN].sprite = 1;
	drawspecs[MAP_VC1|MAP_OWN].click = 1;

	drawspecs[MAP_VC2|MAP_OWN].c.r = 255;
	drawspecs[MAP_VC2|MAP_OWN].c.g = 255;
	drawspecs[MAP_VC2|MAP_OWN].c.b = 255;
	drawspecs[MAP_VC2|MAP_OWN].c.a = 200;
	drawspecs[MAP_VC2|MAP_OWN].texture = -1;
	drawspecs[MAP_VC2|MAP_OWN].sprite = -1;
	drawspecs[MAP_VC2|MAP_OWN].click = 1;

	drawspecs[MAP_VC1|MAP_ALLY].c.r = 255;
	drawspecs[MAP_VC1|MAP_ALLY].c.g = 255;
	drawspecs[MAP_VC1|MAP_ALLY].c.b = 255;
	drawspecs[MAP_VC1|MAP_ALLY].c.a = 200;
	drawspecs[MAP_VC1|MAP_ALLY].texture = 3;
	drawspecs[MAP_VC1|MAP_ALLY].sprite = 1;
	drawspecs[MAP_VC1|MAP_ALLY].click = 1;

	drawspecs[MAP_VC2|MAP_ALLY].c.r = 255;
	drawspecs[MAP_VC2|MAP_ALLY].c.g = 255;
	drawspecs[MAP_VC2|MAP_ALLY].c.b = 255;
	drawspecs[MAP_VC2|MAP_ALLY].c.a = 200;
	drawspecs[MAP_VC2|MAP_ALLY].texture = -1;
	drawspecs[MAP_VC2|MAP_ALLY].sprite = -1;
	drawspecs[MAP_VC2|MAP_ALLY].click = 1;

	drawspecs[MAP_OUTPOST].c.r = 255;
	drawspecs[MAP_OUTPOST].c.g = 0;
	drawspecs[MAP_OUTPOST].c.b = 0;
	drawspecs[MAP_OUTPOST].c.a = 200;
	drawspecs[MAP_OUTPOST].texture = 3;
	drawspecs[MAP_OUTPOST].sprite = 8;
	drawspecs[MAP_OUTPOST].click = 0;

	drawspecs[MAP_OUTPOST|MAP_OWN].c.r = 255;
	drawspecs[MAP_OUTPOST|MAP_OWN].c.g = 255;
	drawspecs[MAP_OUTPOST|MAP_OWN].c.b = 255;
	drawspecs[MAP_OUTPOST|MAP_OWN].c.a = 200;
	drawspecs[MAP_OUTPOST|MAP_OWN].texture = 3;
	drawspecs[MAP_OUTPOST|MAP_OWN].sprite = 8;
	drawspecs[MAP_OUTPOST|MAP_OWN].click = 3;

	drawspecs[MAP_OUTPOST|MAP_ALLY].c.r = 255;
	drawspecs[MAP_OUTPOST|MAP_ALLY].c.g = 255;
	drawspecs[MAP_OUTPOST|MAP_ALLY].c.b = 255;
	drawspecs[MAP_OUTPOST|MAP_ALLY].c.a = 200;
	drawspecs[MAP_OUTPOST|MAP_ALLY].texture = 3;
	drawspecs[MAP_OUTPOST|MAP_ALLY].sprite = 8;
	drawspecs[MAP_OUTPOST|MAP_ALLY].click = 3;

	drawspecs[MAP_WALL1].c.r = 255;
	drawspecs[MAP_WALL1].c.g = 0;
	drawspecs[MAP_WALL1].c.b = 0;
	drawspecs[MAP_WALL1].c.a = 200;
	drawspecs[MAP_WALL1].texture = 3;
	drawspecs[MAP_WALL1].sprite = 1;
	drawspecs[MAP_WALL1].click = 0;

	drawspecs[MAP_WALL2].c.r = 255;
	drawspecs[MAP_WALL2].c.g = 0;
	drawspecs[MAP_WALL2].c.b = 0;
	drawspecs[MAP_WALL2].c.a = 200;
	drawspecs[MAP_WALL2].texture = -1;
	drawspecs[MAP_WALL2].sprite = -1;
	drawspecs[MAP_WALL2].click = 0;

	drawspecs[MAP_WALL1|MAP_OWN].c.r = 255;
	drawspecs[MAP_WALL1|MAP_OWN].c.g = 255;
	drawspecs[MAP_WALL1|MAP_OWN].c.b = 255;
	drawspecs[MAP_WALL1|MAP_OWN].c.a = 200;
	drawspecs[MAP_WALL1|MAP_OWN].texture = 3;
	drawspecs[MAP_WALL1|MAP_OWN].sprite = 0;
	drawspecs[MAP_WALL1|MAP_OWN].click = 2;

	drawspecs[MAP_WALL2|MAP_OWN].c.r = 255;
	drawspecs[MAP_WALL2|MAP_OWN].c.g = 255;
	drawspecs[MAP_WALL2|MAP_OWN].c.b = 255;
	drawspecs[MAP_WALL2|MAP_OWN].c.a = 200;
	drawspecs[MAP_WALL2|MAP_OWN].texture = -1;
	drawspecs[MAP_WALL2|MAP_OWN].sprite = -1;
	drawspecs[MAP_WALL2|MAP_OWN].click = 2;

	drawspecs[MAP_WALL1|MAP_ALLY].c.r = 255;
	drawspecs[MAP_WALL1|MAP_ALLY].c.g = 255;
	drawspecs[MAP_WALL1|MAP_ALLY].c.b = 255;
	drawspecs[MAP_WALL1|MAP_ALLY].c.a = 200;
	drawspecs[MAP_WALL1|MAP_ALLY].texture = 3;
	drawspecs[MAP_WALL1|MAP_ALLY].sprite = 0;
	drawspecs[MAP_WALL1|MAP_ALLY].click = 2;

	drawspecs[MAP_WALL2|MAP_ALLY].c.r = 255;
	drawspecs[MAP_WALL2|MAP_ALLY].c.g = 255;
	drawspecs[MAP_WALL2|MAP_ALLY].c.b = 255;
	drawspecs[MAP_WALL2|MAP_ALLY].c.a = 200;
	drawspecs[MAP_WALL2|MAP_ALLY].texture = -1;
	drawspecs[MAP_WALL2|MAP_ALLY].sprite = -1;
	drawspecs[MAP_WALL2|MAP_ALLY].click = 2;

	drawspecs[MAP_FARM1].c.r = 255;
	drawspecs[MAP_FARM1].c.g = 0;
	drawspecs[MAP_FARM1].c.b = 0;
	drawspecs[MAP_FARM1].c.a = 200;
	drawspecs[MAP_FARM1].texture = 3;
	drawspecs[MAP_FARM1].sprite = 2;
	drawspecs[MAP_FARM1].click = 0;

	drawspecs[MAP_FARM1|MAP_OWN].c.r = 255;
	drawspecs[MAP_FARM1|MAP_OWN].c.g = 255;
	drawspecs[MAP_FARM1|MAP_OWN].c.b = 255;
	drawspecs[MAP_FARM1|MAP_OWN].c.a = 200;
	drawspecs[MAP_FARM1|MAP_OWN].texture = 3;
	drawspecs[MAP_FARM1|MAP_OWN].sprite = 2;
	drawspecs[MAP_FARM1|MAP_OWN].click = 3;

	drawspecs[MAP_FARM1|MAP_ALLY].c.r = 255;
	drawspecs[MAP_FARM1|MAP_ALLY].c.g = 255;
	drawspecs[MAP_FARM1|MAP_ALLY].c.b = 255;
	drawspecs[MAP_FARM1|MAP_ALLY].c.a = 200;
	drawspecs[MAP_FARM1|MAP_ALLY].texture = 3;
	drawspecs[MAP_FARM1|MAP_ALLY].sprite = 2;
	drawspecs[MAP_FARM1|MAP_ALLY].click = 3;

	drawspecs[MAP_HUT].c.r = 255;
	drawspecs[MAP_HUT].c.g = 0;
	drawspecs[MAP_HUT].c.b = 0;
	drawspecs[MAP_HUT].c.a = 200;
	drawspecs[MAP_HUT].texture = 3;
	drawspecs[MAP_HUT].sprite = 8;
	drawspecs[MAP_HUT].click = 0;

	drawspecs[MAP_HUT|MAP_OWN].c.r = 255;
	drawspecs[MAP_HUT|MAP_OWN].c.g = 255;
	drawspecs[MAP_HUT|MAP_OWN].c.b = 255;
	drawspecs[MAP_HUT|MAP_OWN].c.a = 200;
	drawspecs[MAP_HUT|MAP_OWN].texture = 3;
	drawspecs[MAP_HUT|MAP_OWN].sprite = 8;
	drawspecs[MAP_HUT|MAP_OWN].click = 3;

	drawspecs[MAP_HUT|MAP_ALLY].c.r = 255;
	drawspecs[MAP_HUT|MAP_ALLY].c.g = 255;
	drawspecs[MAP_HUT|MAP_ALLY].c.b = 255;
	drawspecs[MAP_HUT|MAP_ALLY].c.a = 200;
	drawspecs[MAP_HUT|MAP_ALLY].texture = 3;
	drawspecs[MAP_HUT|MAP_ALLY].sprite = 8;
	drawspecs[MAP_HUT|MAP_ALLY].click = 3;

	drawspecs[MAP_BARRACKS1].c.r = 255;
	drawspecs[MAP_BARRACKS1].c.g = 0;
	drawspecs[MAP_BARRACKS1].c.b = 0;
	drawspecs[MAP_BARRACKS1].c.a = 200;
	drawspecs[MAP_BARRACKS1].texture = 3;
	drawspecs[MAP_BARRACKS1].sprite = 0;
	drawspecs[MAP_BARRACKS1].click = 0;

	drawspecs[MAP_BARRACKS2].c.r = 255;
	drawspecs[MAP_BARRACKS2].c.g = 0;
	drawspecs[MAP_BARRACKS2].c.b = 0;
	drawspecs[MAP_BARRACKS2].c.a = 200;
	drawspecs[MAP_BARRACKS2].texture = -1;
	drawspecs[MAP_BARRACKS2].sprite = -1;
	drawspecs[MAP_BARRACKS2].click = 0;

	drawspecs[MAP_BARRACKS1|MAP_OWN].c.r = 255;
	drawspecs[MAP_BARRACKS1|MAP_OWN].c.g = 255;
	drawspecs[MAP_BARRACKS1|MAP_OWN].c.b = 255;
	drawspecs[MAP_BARRACKS1|MAP_OWN].c.a = 200;
	drawspecs[MAP_BARRACKS1|MAP_OWN].texture = 3;
	drawspecs[MAP_BARRACKS1|MAP_OWN].sprite = 0;
	drawspecs[MAP_BARRACKS1|MAP_OWN].click = 2;

	drawspecs[MAP_BARRACKS2|MAP_OWN].c.r = 255;
	drawspecs[MAP_BARRACKS2|MAP_OWN].c.g = 255;
	drawspecs[MAP_BARRACKS2|MAP_OWN].c.b = 255;
	drawspecs[MAP_BARRACKS2|MAP_OWN].c.a = 200;
	drawspecs[MAP_BARRACKS2|MAP_OWN].texture = -1;
	drawspecs[MAP_BARRACKS2|MAP_OWN].sprite = -1;
	drawspecs[MAP_BARRACKS2|MAP_OWN].click = 2;

	drawspecs[MAP_BARRACKS1|MAP_ALLY].c.r = 255;
	drawspecs[MAP_BARRACKS1|MAP_ALLY].c.g = 255;
	drawspecs[MAP_BARRACKS1|MAP_ALLY].c.b = 255;
	drawspecs[MAP_BARRACKS1|MAP_ALLY].c.a = 200;
	drawspecs[MAP_BARRACKS1|MAP_ALLY].texture = 3;
	drawspecs[MAP_BARRACKS1|MAP_ALLY].sprite = 0;
	drawspecs[MAP_BARRACKS1|MAP_ALLY].click = 2;

	drawspecs[MAP_BARRACKS2|MAP_ALLY].c.r = 255;
	drawspecs[MAP_BARRACKS2|MAP_ALLY].c.g = 255;
	drawspecs[MAP_BARRACKS2|MAP_ALLY].c.b = 255;
	drawspecs[MAP_BARRACKS2|MAP_ALLY].c.a = 200;
	drawspecs[MAP_BARRACKS2|MAP_ALLY].texture = -1;
	drawspecs[MAP_BARRACKS2|MAP_ALLY].sprite = -1;
	drawspecs[MAP_BARRACKS2|MAP_ALLY].click = 2;

	drawspecs[MAP_GRASSTREE1].c.r = 1;
	drawspecs[MAP_GRASSTREE1].c.g = 42;
	drawspecs[MAP_GRASSTREE1].c.b = 0;
	drawspecs[MAP_GRASSTREE1].c.a = 200;
	drawspecs[MAP_GRASSTREE1].texture = 2;
	drawspecs[MAP_GRASSTREE1].sprite = 0;
	drawspecs[MAP_GRASSTREE1].click = 0;

	drawspecs[MAP_GRASSTREE2].c.r = 1;
	drawspecs[MAP_GRASSTREE2].c.g = 42;
	drawspecs[MAP_GRASSTREE2].c.b = 0;
	drawspecs[MAP_GRASSTREE2].c.a = 200;
	drawspecs[MAP_GRASSTREE2].texture = 2;
	drawspecs[MAP_GRASSTREE2].sprite = 1;
	drawspecs[MAP_GRASSTREE2].click = 0;

	drawspecs[MAP_GRASSTREE3].c.r = 1;
	drawspecs[MAP_GRASSTREE3].c.g = 42;
	drawspecs[MAP_GRASSTREE3].c.b = 0;
	drawspecs[MAP_GRASSTREE3].c.a = 200;
	drawspecs[MAP_GRASSTREE3].texture = 2;
	drawspecs[MAP_GRASSTREE3].sprite = 2;
	drawspecs[MAP_GRASSTREE3].click = 0;

	drawspecs[MAP_GRASSTREE4].c.r = 1;
	drawspecs[MAP_GRASSTREE4].c.g = 42;
	drawspecs[MAP_GRASSTREE4].c.b = 0;
	drawspecs[MAP_GRASSTREE4].c.a = 200;
	drawspecs[MAP_GRASSTREE4].texture = 2;
	drawspecs[MAP_GRASSTREE4].sprite = 3;
	drawspecs[MAP_GRASSTREE4].click = 0;

	drawspecs[MAP_SANDTREE1].c.r = 1;
	drawspecs[MAP_SANDTREE1].c.g = 42;
	drawspecs[MAP_SANDTREE1].c.b = 0;
	drawspecs[MAP_SANDTREE1].c.a = 200;
	drawspecs[MAP_SANDTREE1].texture = 2;
	drawspecs[MAP_SANDTREE1].sprite = 4;
	drawspecs[MAP_SANDTREE1].click = 0;

	drawspecs[MAP_SANDTREE2].c.r = 1;
	drawspecs[MAP_SANDTREE2].c.g = 42;
	drawspecs[MAP_SANDTREE2].c.b = 0;
	drawspecs[MAP_SANDTREE2].c.a = 200;
	drawspecs[MAP_SANDTREE2].texture = 2;
	drawspecs[MAP_SANDTREE2].sprite = 5;
	drawspecs[MAP_SANDTREE2].click = 0;

	drawspecs[MAP_SANDTREE3].c.r = 1;
	drawspecs[MAP_SANDTREE3].c.g = 42;
	drawspecs[MAP_SANDTREE3].c.b = 0;
	drawspecs[MAP_SANDTREE3].c.a = 200;
	drawspecs[MAP_SANDTREE3].texture = 2;
	drawspecs[MAP_SANDTREE3].sprite = 6;
	drawspecs[MAP_SANDTREE3].click = 0;

	drawspecs[MAP_SANDTREE4].c.r = 1;
	drawspecs[MAP_SANDTREE4].c.g = 42;
	drawspecs[MAP_SANDTREE4].c.b = 0;
	drawspecs[MAP_SANDTREE4].c.a = 200;
	drawspecs[MAP_SANDTREE4].texture = 2;
	drawspecs[MAP_SANDTREE4].sprite = 7;
	drawspecs[MAP_SANDTREE4].click = 0;

	drawspecs[MAP_SHRUB1].c.r = 1;
	drawspecs[MAP_SHRUB1].c.g = 42;
	drawspecs[MAP_SHRUB1].c.b = 0;
	drawspecs[MAP_SHRUB1].c.a = 200;
	drawspecs[MAP_SHRUB1].texture = 1;
	drawspecs[MAP_SHRUB1].sprite = 16;
	drawspecs[MAP_SHRUB1].click = 0;

	drawspecs[MAP_SHRUB2].c.r = 1;
	drawspecs[MAP_SHRUB2].c.g = 42;
	drawspecs[MAP_SHRUB2].c.b = 0;
	drawspecs[MAP_SHRUB2].c.a = 200;
	drawspecs[MAP_SHRUB2].texture = 1;
	drawspecs[MAP_SHRUB2].sprite = 18;
	drawspecs[MAP_SHRUB2].click = 0;

	drawspecs[MAP_SHRUB3].c.r = 1;
	drawspecs[MAP_SHRUB3].c.g = 42;
	drawspecs[MAP_SHRUB3].c.b = 0;
	drawspecs[MAP_SHRUB3].c.a = 200;
	drawspecs[MAP_SHRUB3].texture = 1;
	drawspecs[MAP_SHRUB3].sprite = 20;
	drawspecs[MAP_SHRUB3].click = 0;

	drawspecs[MAP_SHRUB4].c.r = 1;
	drawspecs[MAP_SHRUB4].c.g = 42;
	drawspecs[MAP_SHRUB4].c.b = 0;
	drawspecs[MAP_SHRUB4].c.a = 200;
	drawspecs[MAP_SHRUB4].texture = 1;
	drawspecs[MAP_SHRUB4].sprite = 22;
	drawspecs[MAP_SHRUB4].click = 0;
}

drawspec_t *drawspec_get(int i)
{
	if (!drawspec_isinit)
		drawspec_init();
	if (i < 0 || i > 255)
		return &drawspecs[MAP_BLANK];

	return &drawspecs[i];
}
