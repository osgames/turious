/************************************************************************
* main.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

void escape()
{
	if (rtg_state == RTG_STATE_PLAY) {
		rtg_state = RTG_STATE_PAUSED;
		cmd_execf("exec ui.elements(pause.!.*.style.visible) true");
	}else if (rtg_state == RTG_STATE_PAUSED) {
		rtg_state = RTG_STATE_PLAY;
		cmd_execf("exec ui.elements(pause.!.*.style.visible) false");
	}
}

void screenshot()
{
	char name[256];
	char* f = config_get("wm.capture_format");
	if (!f)
		f = "jpg";
	snprintf(name,256,"turious-screenshot-%u.%s",time_ticks(),f);
	wm_capture(name);
	rtprintf(CN_INFO "saved screenshot to '%s'",file_path(name));
}

int main(int argc, char** argv)
{
	int s;
	rtg_path_set(GAMEDATA);
	rtg_path_set(NULL);
	rtg_path_add_home();
	config_set_int("map.seed",0);
	config_set_int("map.random_seed",1);
	config_set_int("game.ais",5);
	config_set_int("game.turn_time",60);
	config_set_int("game.difficulty",1);
	rtg_init(argc,argv,RTG_INIT_ALL,"Turious");

	ui_xml_load("main.ui");

	event_create("menu","esc",NULL,escape,NULL,NULL);
	event_create("select","mouse-1",NULL,village_select,NULL,NULL);
	event_create("map","m",NULL,map_toggle,NULL,NULL);
	event_create("left","a",NULL,NULL,NULL,village_move_left);
	event_create("right","d",NULL,NULL,NULL,village_move_right);
	event_create("down","s",NULL,NULL,NULL,village_move_down);
	event_create("up","w",NULL,NULL,NULL,village_move_up);
	event_create("home","h",NULL,village_gohome,NULL,NULL);
	event_create("screenshot","x",NULL,screenshot,NULL,NULL);

	gui_init();

	sound_load_music("menu.wav","menu");
	sound_load_music("win.wav","win");
	sound_load_effect("battle.wav","battle");
	sound_load_effect("horn.wav","scout");

	s = rtg_state;

	sound_play_music("menu");

	while (rtg_state) {
		render_clear();

		if (s != rtg_state) {
			switch (rtg_state) {
			case RTG_STATE_MENU:
				client_disconnect();
				sound_stop_effects(0);
				sound_play_music("menu");
				break;
			default:
				sound_stop_music(2);
				texteffect_clear();
			}
			s = rtg_state;
		}

		client_main();
		village_draw();
		texteffect_draw();

		printf2d(10,10,1,12,"FPS: %d",wm_res.fps);
		if (rtg_state == RTG_STATE_PLAY)
			printf2d(10,25,1,12,"Seed: %d",seed);

		render();
	}

	rtg_exit();

	return 0;
}
