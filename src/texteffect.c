/************************************************************************
* texteffect.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2014 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

typedef struct texteffect_s {
	struct texteffect_s *prev;
	struct texteffect_s *next;
	char* str;
	float state;
} texteffect_t;

static int sizes[10] = {
	40,
	42,
	42,
	42,
	41,
	40,
	38,
	35,
	31,
	26
};

static texteffect_t *texts = NULL;

void texteffect_add(char* str)
{
	texteffect_t *e = malloc(sizeof(texteffect_t));
	e->str = strdup(str);
	e->state = 60.0;

	texts = list_push(&texts,e);
}

void texteffect_draw()
{
	texteffect_t *e;
	int x;
	int y;
	int w;
	int h;
	int s;
	if (!texts)
		return;

	e = texts;

	s = e->state/6;
	s = sizes[s];

	w = print_length(0,s,e->str);
	h = print_height(0,s,e->str);
	x = (wm_res.width-w)/2;
	y = (wm_res.height-h)/2;
	print2d(x,y,0,s,e->str);

	e->state -= anim_factor;
	if (e->state > 0.0)
		return;

	e = list_pull(&texts);
	free(e->str);
	free(e);
}

void texteffect_clear()
{
	texteffect_t *e;
	while ((e = list_pull(&texts))) {
		free(e->str);
		free(e);
	}
}
