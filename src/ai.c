/************************************************************************
* ai.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

static array_t *ais = NULL;

static ai_t *ai_find(int id)
{
	int i;
	ai_t **ai = ais->data;
	for (i=0; i<ais->length; i++) {
		if (ai[i]->id == id)
			return ai[i];
	}

	return NULL;
}

static int ai_attack(ai_t *a)
{
	action_t *act;
	int pc;
	int t;
	int i;

	pc = player_count();
	t = rand_range(1,pc);

	/* make sure the target isn't defeated or themselves */
	while (t == a->id || player_is_defeated(t)) {
		t = rand_range(1,pc);
	}

	act = malloc(sizeof(action_t));
	act->owner = a->id;
	act->target = t;
	act->action = ACTION_ATTACK;
	for (i=0; i<UNIT_COUNT; i++) {
		act->units[i] = 0;
	}
	act->units[UNIT_SOLDIER] = rand_range(0,a->units[UNIT_SOLDIER]);
	act->units[UNIT_BRIGADIER] = rand_range(0,a->units[UNIT_BRIGADIER]);

	game_push_action(act);

	return 1;
}

static int ai_train(ai_t *a)
{
	action_t *act;
	int i;
	int s = game_get_cost((playerdata_t*)a,UNIT_SOLDIER,1);

	/* select unit type */
	if (s < a->credits) {
		if (a->units[UNIT_SOLDIER] < a->units[UNIT_FARMER]*10) {
			s = UNIT_SOLDIER;
		}else if (a->units[UNIT_SOLDIER] > a->units[UNIT_FARMER]*10) {
			s = UNIT_FARMER;
		}else if (a->units[UNIT_SOLDIER] < a->units[UNIT_FARMER]) {
			s = UNIT_SOLDIER;
		}else if (a->units[UNIT_SCOUT] < 10*a->strength && a->credits > game_get_cost((playerdata_t*)a,UNIT_SCOUT,1)) {
			s = UNIT_SCOUT;
		}else if (rand_range(0,1)) {
			s = UNIT_SOLDIER;
		}else{
			s = UNIT_FARMER;
		}
	}else{
		s = UNIT_FARMER;
	}

	/* credit deduction */
	a->credits -= game_get_cost((playerdata_t*)a,s,1);

	act = malloc(sizeof(action_t));
	act->owner = a->id;
	act->action = ACTION_TRAIN;
	for (i=0; i<UNIT_COUNT; i++) {
		act->units[i] = 0;
	}
	act->units[s] = 1;

	game_push_action(act);

	return 1;
}

static int ai_promote(ai_t *a)
{
	action_t *act;
	int i;
	int s = game_get_cost((playerdata_t*)a,UNIT_MASTER,1);

	/* select unit type */
	if (rand_range(0,1)) {
		s = UNIT_BRIGADIER;
	}else{
		s = UNIT_MASTER;
	}

	/* credit deduction */
	a->credits -= game_get_cost((playerdata_t*)a,s,1);

	act = malloc(sizeof(action_t));
	act->owner = a->id;
	act->action = ACTION_TRAIN;
	for (i=0; i<UNIT_COUNT; i++) {
		act->units[i] = 0;
	}
	act->units[s] = 1;

	game_push_action(act);

	return 1;
}

static int ai_upgrade(ai_t *a)
{
	action_t *act;
	int i;

	/* credit deduction */
	a->credits -= game_get_cost((playerdata_t*)a,ACTION_UPGRADE,0);

	act = malloc(sizeof(action_t));
	act->owner = a->id;
	act->action = ACTION_UPGRADE;
	for (i=0; i<UNIT_COUNT; i++) {
		act->units[i] = 0;
	}

	game_push_action(act);

	return 1;
}

static int (*ai_opts[])(ai_t*) = {
	ai_attack,
	ai_train,
	ai_promote,
	ai_upgrade
};

/* calculate what a player can do, then choose an option and do it */
static int ai_doturn(ai_t *a, int turn)
{
	int o = 0;
	int r = 0;
	int i;
	int m;
	int opts[4] = {0,0,0,0}; /* attack, train, promote, upgrade */

	/* find out what's possible */
	if (turn > 5/game.difficulty && a->units[UNIT_SOLDIER] > game.difficulty)
		opts[0] = 1;
	if (a->credits > game_get_cost((playerdata_t*)a,UNIT_FARMER,1))
		opts[1] = 1;
	if (a->credits > 500 && a->units[UNIT_VILLAGER] > 200)
		opts[2] = 1;
	if (a->credits > game_get_cost((playerdata_t*)a,ACTION_UPGRADE,0) && a->units[UNIT_VILLAGER] > ((a->strength+1)*100))
		opts[3] = rand_range(0,1);

	m = player_count();

	for (i=0; i<m; i++) {
		o = rand_range(0,3);
		if (!opts[o])
			continue;

		/* do action */
		r = ai_opts[o](a);
		if (r)
			break;
	}

	if (!r)
		return ai_train(a);

	return r;
}

static void ai_data(int id, int c, int s, int u[UNIT_COUNT])
{
	int i;
	ai_t *a = ai_find(id);
	if (!a)
		return;

	a->credits = c;
	a->strength = s;
	for (i=0; i<UNIT_COUNT; i++) {
		a->units[i] = u[i];
	}
}

static void ai_state(int id, int state)
{
	ai_t *a = ai_find(id);
	if (!a)
		return;

	if (state == PLAYER_DEFEATED)
		a->defeated = 1;
}

int ai_init(int count)
{
	ai_t *a;
	comset_t *c;
	int i;
	int k;
	if (count < 1)
		return 0;
	if (!ais)
		ais = array_create(RTG_TYPE_PTR);

	for (i=0; i<count; i++) {
		c = malloc(sizeof(comset_t));
		c->type = CLIENT_AI;
		c->data = ai_data;
		c->msg = NULL;
		c->sig = NULL;
		c->map = NULL;
		c->state = ai_state;
		c->init = NULL;
		c->unit = NULL;

		a = malloc(sizeof(ai_t));
		a->last = 0;
		a->credits = 0;
		a->strength = 0;
		a->upgrading = 0;
		a->defeated = 0;

		for (k=0; k<UNIT_COUNT; k++) {
			a->units[k] = 0;
		}

		array_push_ptr(ais,a);

		a->id = player_connect(NULL,c);
	}

	return 0;
}

void ai_exit()
{
	ai_t *a;
	if (!ais || !ais->length)
		return;

	while ((a = array_pop_ptr(ais))) {
		player_disconnect(a->id);
		free(a);
	}
}

/* do each ai players turn */
int ai_doturns(int turn)
{
	int k;
	int i;
	ai_t *a = NULL;
	if (!ais || !ais->length)
		return 0;

	if (turn < 0) {
		k = 0;
		for (i=0; i<ais->length; i++) {
			a = array_get_ptr(ais,i);
			if (a && !a->defeated && !rand_range(0,1)) {
				ai_doturn(a,turn);
				k++;
			}
		}
		return k;
	}else{
		for (k=0; k<ais->length; k++) {
			i = rand_range(0,ais->length-1);
			a = array_get_ptr(ais,i);
			if (a && !a->defeated && a->last != turn)
				break;
		}
		if (!a || a->defeated || a->last == turn)
			return 0;

		if (ai_doturn(a,turn))
			a->last = turn;
		return 1;
	}
	return 0;
}
