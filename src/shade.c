/************************************************************************
* shade.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2014 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

void shade_create(int x, int y)
{
	playerdata_t *p;
	int i;
	int s = x*y;
	for (i=0; i<game.players->length; i++) {
		p = array_get_ptr(game.players,i);
		if (!p)
			continue;
		p->shade = malloc(s);
		memset(p->shade,SHADE_HIDDEN,s);
	}
}

void shade_reset()
{
	playerdata_t *p;
	int i;
	int s;
	int x;
	int y;
	int ix;
	int iy;
	for (i=0; i<game.players->length; i++) {
		p = array_get_ptr(game.players,i);
		if (!p)
			continue;
		for (s=0; s<mapgen.l; s++) {
			if (p->shade[s] == SHADE_BRIGHT)
				p->shade[s] = SHADE_DARK;
		}

		s = mapgen_find(p->id,MAP_VC1);
		x = s%mapgen.w;
		y = s/mapgen.w;
		for (ix=-5; ix<2; ix++) {
			for (iy=-5; iy<2; iy++) {
				shade_set(p->id,x+ix,y+iy,SHADE_BRIGHT);
			}
		}

		s = mapgen_find(p->id,MAP_BARRACKS1);
		x = s%mapgen.w;
		y = s/mapgen.w;
		for (ix=-3; ix<3; ix++) {
			for (iy=-3; iy<3; iy++) {
				shade_set(p->id,x+ix,y+iy,SHADE_BRIGHT);
			}
		}

		s = mapgen_find(p->id,MAP_FARM1);
		x = s%mapgen.w;
		y = s/mapgen.w;
		for (ix=-2; ix<3; ix++) {
			for (iy=-2; iy<3; iy++) {
				shade_set(p->id,x+ix,y+iy,SHADE_BRIGHT);
			}
		}
	}
}

void shade_set(int id, int x, int y, unsigned char v)
{
	int s;
	playerdata_t *p;

	if (x > mapgen.w || y > mapgen.h)
		return;

	p = array_get_ptr(game.players,id-1);

	s = x+(y*mapgen.w);

	p->shade[s] = v;
}

unsigned char shade_get(int id, int x, int y)
{
	int s;
	playerdata_t *p;

	if (x > mapgen.w || y > mapgen.h)
		return SHADE_HIDDEN;

	p = array_get_ptr(game.players,id-1);
	if (!p)
		return SHADE_HIDDEN;

	s = x+(y*mapgen.w);

	return p->shade[s];
}
