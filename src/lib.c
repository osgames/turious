/************************************************************************
* lib.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

/* return a random number between low and high */
int rand_range(int low, int high)
{
	static int rand_seeded = 0;
	int x;
	int n = (high-low)+1;
	int rand_limit;
	int rand_excess;

	if (low >= high)
		return low;

	if (!rand_seeded) {
		srand((unsigned int)time(NULL));
		rand_seeded = 1;
	}

	rand_excess = ((int64_t)RAND_MAX + 1) % n;
	rand_limit = RAND_MAX - rand_excess;
	while ((x = rand()) > rand_limit);

	return (x % n)+low;
}

/* get the sprite face direction for b -> e */
int direction(int bx, int by, int ex, int ey)
{
	if (bx < ex) {
		if (by < ey)
			return 6;
		if (by > ey)
			return 4;
		return 5;
	}
	if (bx > ex) {
		if (by < ey)
			return 0;
		if (by > ey)
			return 2;
		return 1;
	}
	if (by < ey)
		return 7;
	return 3;
}

/* get the tile distance between b -> e */
int distance(int bx, int by, int ex, int ey)
{
	int dx = bx-ex;
	int dy = by-ey;
	if (dx < 0)
		dx *= -1;
	if (dy < 0)
		dy *= -1;
	return dx+dy;
}
