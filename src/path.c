/************************************************************************
* path.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

typedef struct path_node_s {
	int x;
	int y;
	int g;
	int h;
	struct path_node_s *p;
} path_node_t;

static int path_h(int x, int y, int ex, int ey)
{
	int hx;
	int hy;

	hx = x-ex;
	if (hx < 0)
		hx *= -1;
	hy = y-ey;
	if (hy < 0)
		hy *= -1;

	return (hx+hy)*10;
}

void path_free(array_t *a)
{
	int i;
	path_node_t *n;
	for (i=0; i<a->length; i++) {
		n = array_get_ptr(a,i);
		if (!n)
			continue;
		free(n);
	}
	array_free(a);
}

path_node_t *path_node(path_node_t *p, int x, int y, int ex, int ey)
{
	path_node_t *n = malloc(sizeof(path_node_t));
	n->p = p;
	n->x = x;
	n->y = y;
	n->g = 0;
	n->h = path_h(x,y,ex,ey);
	if (p) {
		if (p->x == x || p->y == y) {
			n->g = p->g + 10;
		}else{
			n->g = p->g + 14;
		}
	}

	return n;
}

path_node_t *path_node_cpy(path_node_t *n)
{
	path_node_t *c = malloc(sizeof(path_node_t));
	c->p = n->p;
	c->x = n->x;
	c->y = n->y;
	c->g = n->g;
	c->h = n->h;

	return c;
}

int path_find_node(array_t *a, int x, int y)
{
	int i;
	path_node_t *n;
	if (!a)
		return -1;

	for (i=0; i<a->length; i++) {
		n = array_get_ptr(a,i);
		if (!n)
			continue;
		if (n->x == x && n->y == y)
			return i;
	}

	return -1;
}

int path_find_lowest(array_t *a)
{
	int i;
	int l = -1;
	path_node_t *n;
	path_node_t *ln;
	if (!a)
		return -1;

	for (i=0; i<a->length; i++) {
		n = array_get_ptr(a,i);
		if (!n)
			continue;
		if (l<0) {
			l = i;
			ln = n;
			continue;
		}
		if ((n->g+n->h) < (ln->g+ln->h)) {
			l = i;
			ln = n;
		}
	}

	return l;
}

int path_collide(int x, int y)
{
	mapnode_t *d = &mapgen.data[x+(mapgen.w*y)];
	switch (d->detail) {
	case MAP_GRASSTREE1:
	case MAP_GRASSTREE2:
	case MAP_GRASSTREE3:
	case MAP_GRASSTREE4:
	case MAP_SANDTREE1:
	case MAP_SANDTREE2:
	case MAP_SANDTREE3:
	case MAP_SANDTREE4:
		return -1;
		break;
	case MAP_VC1:
	case MAP_VC2:
	case MAP_BARRACKS1:
	case MAP_BARRACKS2:
	case MAP_FARM1:
	case MAP_HUT:
	case MAP_OUTPOST:
	case MAP_WALL1:
	case MAP_WALL2:
		return -1;
		break;
	default:;
	}
	switch (d->tile) {
	case MAP_GRASS:
	case MAP_SAND:
	case MAP_PATH:
	case MAP_SHRUB1:
	case MAP_SHRUB2:
	case MAP_SHRUB3:
	case MAP_SHRUB4:
		return 0;
		break;
	case MAP_WATER:
		return -2;
		break;
	default:;
	}
	return 1;
}

void path_neighbours(array_t *open, array_t *closed, path_node_t *current, int ex, int ey)
{
	int x = current->x;
	int y = current->y;
	path_node_t *n;
	path_node_t *en;
	int e;
	int c;
	int xo;
	int yo;
	int xc;
	int yc;
	for (xo=-1; xo<2; xo++) {
		xc = x+xo;
		if (xc < 0 || xc >= mapgen.w)
			continue;
		for (yo=-1; yo<2; yo++) {
			/* ignore current */
			if (!xo && !yo)
				continue;
			yc = y+yo;
			/* ignore non-existant tiles */
			if (yc < 0 || yc >= mapgen.h)
				continue;
			/* ignore already checked */
			if (path_find_node(closed,xc,yc) > -1)
				continue;
			/* ignore non-walkable */
			c = path_collide(xc,yc);
			if (c == 1)
				continue;
			n = path_node(current,xc,yc,ex,ey);
			if (c == -1)
				n->g += 500;
			if (c == -2)
				n->g += 5000;
			e = path_find_node(open,xc,yc);
			if (e > -1) {
				en = array_get_ptr(open,e);
				if (n->g < en->g) {
					array_set_ptr(open,n,e);
					free(en);
				}else{
					free(n);
				}
				continue;
			}
			array_push_ptr(open,n);
		}
	}
}

array_t *path_find(int x, int y, int ex, int ey)
{
	array_t *path = array_create(RTG_TYPE_PTR);
	array_t *open = array_create(RTG_TYPE_PTR);
	array_t *closed = array_create(RTG_TYPE_PTR);
	path_node_t *c = path_node(NULL,x,y,ex,ey);
	int l;
	array_push_ptr(open,c);

	while (c && !(c->x == ex && c->y == ey)) {
		path_neighbours(open,closed,c,ex,ey);

		l = path_find_node(open,c->x,c->y);
		array_set_ptr(open,NULL,l);
		array_push_ptr(closed,c);

		l = path_find_lowest(open);
		c = array_get_ptr(open,l);
	}
	if (c && c->x == ex && c->y == ey) {
		int *p;
		array_t *tp = array_create(RTG_TYPE_PTR);
		while (c) {
			p = malloc(sizeof(int)*2);
			p[0] = c->x;
			p[1] = c->y;
			array_push_ptr(tp,p);
			c = c->p;
		}
		while ((p = array_pop_ptr(tp))) {
			array_push_ptr(path,p);
		}
		array_free(tp);
	}

	path_free(open);
	path_free(closed);

	if (path->length)
		return path;

	array_free(path);
	return NULL;
}

array_t *path_copy(array_t *o)
{
	array_t *n;
	int *p1;
	int *p2;
	int i;

	n = array_create(RTG_TYPE_PTR);
	for (i=0; i<o->length; i++) {
		p1 = array_get_ptr(o,i);
		if (!p1)
			continue;
		p2 = malloc(sizeof(int)*2);
		p2[0] = p1[0];
		p2[1] = p1[1];
		array_push_ptr(n,p2);
	}

	return n;
}

#if 0
void path_test()
{
	int i;
	array_t *p;
	int x = 10;
	int y = 10;
	int ex = 50;
	int ey = 20;
	int *t;

	rtprintf("find path (%d,%d) (%d,%d)", x,y,ex,ey);

	p = path_find(x,y,ex,ey);
	if (!p) {
		rtprintf("no path");
		return;
	}
	if (!p->length) {
		array_free(p);
		rtprintf("empty path");
		return;
	}
	rtprintf("path length %d",p->length);
	for (i=0; i<p->length; i++) {
		t = array_get_ptr(p,i);
		rtprintf("path %d: (%d,%d)",i,t[0],t[1]);
		village.tiles[t[0]][t[1]].overlay[0] = 1;
		village.tiles[t[0]][t[1]].overlay[1] = 42;
	}
	path_free(p);
}
#endif
