/************************************************************************
* mapgen.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2012 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

#include <stdarg.h>

unsigned char tiletypes[] = {
	MAP_GRASS,
	MAP_GRASS,
	MAP_WATER,
	MAP_GRASSTREE1,
	MAP_GRASS,
	MAP_SAND,
	MAP_GRASS,
	MAP_SANDTREE1,
};
unsigned char grasstreetypes[] = {
	MAP_BLANK,
	MAP_GRASSTREE1,
	MAP_GRASSTREE2,
	MAP_GRASSTREE3,
	MAP_GRASSTREE4,
	MAP_BLANK,
	MAP_BLANK,
	MAP_SHRUB1,
	MAP_SHRUB2,
	MAP_SHRUB3,
	MAP_SHRUB4,
	MAP_BLANK,
};
unsigned char sandtreetypes[] = {
	MAP_BLANK,
	MAP_SANDTREE1,
	MAP_BLANK,
	MAP_SANDTREE2,
	MAP_BLANK,
	MAP_SANDTREE3,
	MAP_BLANK,
	MAP_SANDTREE4,
	MAP_BLANK
};

struct mapplayer {
	int id;
	int vc[9];
	int barracks[4];
	array_t *farm;
};

typedef struct mapfarm_s {
	int x;
	int y;
	int p;
	int type;
} mapfarm_t;

mapgen_t mapgen = {
	0,
	0,
	0,
	NULL,
	0,
	NULL
};

static int mapgen_check_radius(int rad, int x, int y, unsigned char type,  ...)
{
	int min_x = x-rad;
	int min_y = y-rad;
	int max_x = x+rad;
	int max_y = y+rad;
	int px;
	int py;
	int i;
	unsigned char *l;
	mapnode_t *t;
	int c = 1;
	va_list ap;
	if (min_x < 0)
		min_x = 0;
	if (min_y < 0)
		min_y = 0;
	if (max_x >= mapgen.w)
		max_x = mapgen.w-1;
	if (max_y >= mapgen.h)
		max_y = mapgen.h-1;

	va_start(ap,type);
	c = 1;
	while (va_arg(ap,unsigned int) != 0) {
		c++;
	}
	va_end(ap);
	l = alloca(c);
	l[0] = type;
	va_start(ap,type);
	c = 1;
	while ((l[c] = va_arg(ap,unsigned int)) != 0) {
		c++;
	}
	va_end(ap);

	for (px=min_x; px<=max_x; px++) {
		for (py=min_y; py<=max_y; py++) {
			t = &mapgen.data[(py*mapgen.w)+px];
			for (i=0; i<c; i++) {
				if (
					t->tile == l[i]
					&& (
						t->detail == MAP_BLANK
						|| t->detail == MAP_SHRUB1
						|| t->detail == MAP_SHRUB2
						|| t->detail == MAP_SHRUB3
						|| t->detail == MAP_SHRUB4
						|| t->detail == MAP_GRASSTREE1
						|| t->detail == MAP_GRASSTREE2
						|| t->detail == MAP_GRASSTREE3
						|| t->detail == MAP_GRASSTREE4
					)
				)
					break;
			}
			if (i >= c)
				return 1;
		}
	}

	return 0;
}

mapnode_t *mapgen_get(int x, int y)
{
	if (x < 0 || x >= mapgen.w || y < 0 || y >= mapgen.h)
		return NULL;

	return &mapgen.data[(y*mapgen.w)+x];
}

int mapgen_get_detail(int x, int y)
{
	if (x < 0 || x >= mapgen.w || y < 0 || y >= mapgen.h)
		return MAP_BLANK;

	return mapgen.data[(y*mapgen.w)+x].detail;
}

int mapgen_get_tile(int x, int y)
{
	if (x < 0 || x >= mapgen.w || y < 0 || y >= mapgen.h)
		return MAP_BLANK;

	return mapgen.data[(y*mapgen.w)+x].tile;
}

int mapgen_init(int players)
{
	int w = players*30;
	int h = w*1.6;
	int l = w*h;
	int x;
	int y;
	int ix;
	int iy;
	int i;
	int k;
	int t;
	int v;
	int o;
	int sd;
	char* s;
	int *p;
	mapnode_t *n;
	mapfarm_t *f;

	mapgen.w = w;
	mapgen.h = h;
	mapgen.l = l;

	s = config_get("map.random_seed");
	if (!strcmp(s,"0")) {
		sd = config_get_int("map.seed");
	}else{
		sd = rand();
		config_set_int("map.seed",sd);
	}
	noise_seed(sd);

	mapgen.pc = players;

	rtprintf(CN_DEBUG "map 1");

	mapgen.data = malloc(l*4);
	for (y=0; y<h; y++) {
		l = y*w;
		for (x=0; x<w; x++) {
			i = noise(x,y);
			n = &mapgen.data[l+x];
			n->shade = SHADE_DARK;
			n->tile = tiletypes[i];
			n->detail = MAP_BLANK;
			n->height = 0;
			if (n->tile == MAP_GRASSTREE1) {
				n->tile = MAP_GRASS;
				v = noise2d(x,y)*20.0;
				if (v<0)
					v *= -1;
				n->detail = grasstreetypes[v%12];
			}
			if (n->tile == MAP_SANDTREE1) {
				n->tile = MAP_SAND;
				v = noise2d(x,y)*20.0;
				if (v<0)
					v *= -1;
				if (v%2 == 0)
					n->detail = sandtreetypes[v%9];
			}
		}
	}

	shade_create(mapgen.w,mapgen.h);

	l = w*h;
	rtprintf(CN_DEBUG "map 2");

	p = alloca(players);
	for (i=0; i<players; i++) {
		p[i] = i+1;
	}
	rtprintf(CN_DEBUG "map 3");

	for (i=players-1; i>0; i--) {
		o = rand_range(0,i);
		t = p[o];
		p[o] = p[i];
		p[i] = t;
	}
	rtprintf(CN_DEBUG "map 4");

	mapgen.players = malloc(sizeof(struct mapplayer)*players);
	for (i=0; i<players; i++) {
		mapgen.players[i].farm = array_create(RTG_TYPE_PTR);
		mapgen.players[i].id = p[i];
		rtprintf(CN_DEBUG "map player %d start",i);
		t = (i*48)+15;
		do {
			y = rand_range(t-10,t+10);
			x = rand_range(10,w-10);
		} while (
			mapgen.data[(y*w)+x].tile != MAP_GRASS
			|| mapgen_check_radius(3,x,y,MAP_GRASS,MAP_SAND,0)
		);
		v = x;
		k = 0;
		for (ix=1; ix>-2; ix--) {
			for (iy=1; iy>-2; iy--) {
				mapgen.players[i].vc[k] = (x+ix)+((y+iy)*w);
				mapgen.data[mapgen.players[i].vc[k]].detail = MAP_VC2;
				k++;
			}
		}
		mapgen.data[mapgen.players[i].vc[0]].detail = MAP_VC1;

		rtprintf(CN_DEBUG "map player %d vc",i);
		o = 4;
		do {
			y = rand_range(t-o,t+o);
			x = rand_range(v-o,v+o);
			o++;
		} while (
			mapgen.data[(y*w)+x].tile != MAP_GRASS
			|| mapgen_check_radius(2,x,y,MAP_GRASS,MAP_SAND,0)
			|| (
				mapgen.players[i].vc[0] > (y*w)+(x-5)
				&& mapgen.players[i].vc[0] < (y*w)+(x+5)
				&& mapgen.players[i].vc[0] > ((y-5)*w)+x
				&& mapgen.players[i].vc[0] < ((y+5)*w)+x
			)
		);
		mapgen.players[i].barracks[0] = ((y+1)*w)+(x+1);
		mapgen.players[i].barracks[1] = ((y+1)*w)+x;
		mapgen.players[i].barracks[2] = (y*w)+(x+1);
		mapgen.players[i].barracks[3] = (y*w)+x;
		mapgen.data[mapgen.players[i].barracks[0]].detail = MAP_BARRACKS1;
		mapgen.data[mapgen.players[i].barracks[1]].detail = MAP_BARRACKS2;
		mapgen.data[mapgen.players[i].barracks[2]].detail = MAP_BARRACKS2;
		mapgen.data[mapgen.players[i].barracks[3]].detail = MAP_BARRACKS2;

		rtprintf(CN_DEBUG "map player %d barracks",i);
		o = 4;
		do {
			y = rand_range(t-o,t+o);
			x = rand_range(v-o,v+o);
			o++;
		} while (
			mapgen.data[(y*w)+x].tile != MAP_GRASS
			|| mapgen_check_radius(1,x,y,MAP_GRASS,MAP_SAND,0)
			|| (
				mapgen.players[i].vc[0] > (y*w)+(x-5)
				&& mapgen.players[i].vc[0] < (y*w)+(x+5)
				&& mapgen.players[i].vc[0] > ((y-5)*w)+x
				&& mapgen.players[i].vc[0] < ((y+5)*w)+x
			) || (
				mapgen.players[i].barracks[0] > (y*w)+(x-4)
				&& mapgen.players[i].barracks[0] < (y*w)+(x+4)
				&& mapgen.players[i].barracks[0] > ((y-4)*w)+x
				&& mapgen.players[i].barracks[0] < ((y+4)*w)+x
			)
		);
		for (ix=-1; ix<1; ix++) {
			for (iy=-1; iy<1; iy++) {
				f = malloc(sizeof(mapfarm_t));
				f->x = x+ix;
				f->y = y+iy;
				f->p = (x+ix)+((y+iy)*w);
				f->type = MAP_FARM1;
				mapgen.data[v].detail = MAP_FARM1;
				array_push_ptr(mapgen.players[i].farm,f);
			}
		}

		rtprintf(CN_DEBUG "map player %d end",i);
	}
	rtprintf(CN_DEBUG "map 5");

	return 0;
}

void mapgen_free()
{
	if (mapgen.data)
		free(mapgen.data);
	mapgen.data = NULL;
	if (mapgen.players)
		free(mapgen.players);
	mapgen.players = NULL;
}

void mapgen_send()
{
	int p;
	int i;
	int k;
	mapfarm_t **f;
	int l = mapgen.w*mapgen.h;
	int x;
	int y;
	mapnode_t *d = alloca(l*4);
	memcpy(d,mapgen.data,l*4);
	for (p=0; p<mapgen.pc; p++) {
		if (player_is_ai(mapgen.players[p].id))
			continue;

		memset(d,0,l*4);

		for (x=0; x<mapgen.w; x++) {
			for (y=0; y<mapgen.h; y++) {
				i = shade_get(mapgen.players[p].id,x,y);
				if (i == SHADE_HIDDEN)
					continue;
				k = x+(y*mapgen.w);
				d[k] = mapgen.data[k];
				d[k].shade = i;
			}
		}
		d[mapgen.players[p].vc[0]].detail = (MAP_VC1|MAP_OWN);
		d[mapgen.players[p].vc[1]].detail = (MAP_VC2|MAP_OWN);
		d[mapgen.players[p].vc[2]].detail = (MAP_VC2|MAP_OWN);
		d[mapgen.players[p].vc[3]].detail = (MAP_VC2|MAP_OWN);
		d[mapgen.players[p].vc[4]].detail = (MAP_VC2|MAP_OWN);
		d[mapgen.players[p].vc[5]].detail = (MAP_VC2|MAP_OWN);
		d[mapgen.players[p].vc[6]].detail = (MAP_VC2|MAP_OWN);
		d[mapgen.players[p].vc[7]].detail = (MAP_VC2|MAP_OWN);
		d[mapgen.players[p].vc[8]].detail = (MAP_VC2|MAP_OWN);
		d[mapgen.players[p].barracks[0]].detail = (MAP_BARRACKS1|MAP_OWN);
		d[mapgen.players[p].barracks[1]].detail = (MAP_BARRACKS2|MAP_OWN);
		d[mapgen.players[p].barracks[2]].detail = (MAP_BARRACKS2|MAP_OWN);
		d[mapgen.players[p].barracks[3]].detail = (MAP_BARRACKS2|MAP_OWN);

		f = mapgen.players[p].farm->data;
		for (k=0; k<mapgen.players[p].farm->length; k++) {
			d[f[k]->p].detail = (f[k]->type|MAP_OWN);
		}

		player_map(mapgen.players[p].id,d,mapgen.w,mapgen.h);
	}
}

int mapgen_find(int player, int type)
{
	int i;
	for (i=0; i<mapgen.pc; i++) {
		if (mapgen.players[i].id == player)
			break;
	}
	if (i >= mapgen.pc)
		return -1;

	switch (type) {
	case MAP_VC1:
		return mapgen.players[i].vc[0];
		break;
	case MAP_BARRACKS1:
		return mapgen.players[i].barracks[0];
		break;
	case MAP_BARRACKS2:
		return mapgen.players[i].barracks[1];
		break;
	case MAP_FARM1:
	{
		mapfarm_t *f = array_get_ptr(mapgen.players[i].farm,0);
		if (f)
			return f->p;
		break;
	}
	}

	return -1;
}
