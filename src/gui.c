/************************************************************************
* gui.c
* turious - turn based strategy game
* Copyright (C) Lisa Milne 2014 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

int gui_sound_show(widget_t *w)
{
	if (sound_state(-1) == 1) {
		ui_widget_value(w,"1");
	}else{
		ui_widget_value(w,"0");
	}
	return EVENT_HANDLED;
}
int gui_sound_click(widget_t *w)
{
	char* v = ui_widget_value(w,NULL);
	cmd_execf("set sound.enabled %s",v);
	return EVENT_HANDLED;
}
int gui_music_show(widget_t *w)
{
	int v = sound_volume_music(-1);
	ui_widget_value(w,"%d",v);
	return EVENT_HANDLED;
}
int gui_music_click(widget_t *w)
{
	char* v = ui_widget_value(w,NULL);
	cmd_execf("set sound.music.volume %s",v);
	return EVENT_HANDLED;
}
int gui_effects_show(widget_t *w)
{
	int v = sound_volume_effects(-1);
	ui_widget_value(w,"%d",v);
	return EVENT_HANDLED;
}
int gui_effects_click(widget_t *w)
{
	char* v = ui_widget_value(w,NULL);
	cmd_execf("set sound.effects.volume %s",v);
	return EVENT_HANDLED;
}
int gui_controls_show(widget_t *w)
{
	ctrl_create(w,320,100);
	return EVENT_HANDLED;
}
int gui_controls_hide(widget_t *w)
{
	ctrl_clear(w);
	return EVENT_HANDLED;
}
int gui_video_show(widget_t *w)
{
	char* v;

	v = config_get("wm.fullscreen");
	ui_widget_set_value("video.check.fullscreen",v);

	v = config_get("wm.framecap");
	ui_widget_set_value("video.num.cap",v);

	v = config_get("wm.width");
	ui_widget_set_value("video.num.width",v);

	v = config_get("wm.height");
	ui_widget_set_value("video.num.height",v);

	v = config_get("gl.anisotropic");
	ui_widget_set_value("video.check.ani",v);

	v = config_get("gl.bilinear");
	ui_widget_set_value("video.check.bil",v);

	v = config_get("gl.trilinear");
	ui_widget_set_value("video.check.tri",v);

	v = config_get("gl.mipmaps");
	ui_widget_set_value("video.check.mip",v);

	return EVENT_HANDLED;
}
int gui_video_save(widget_t *w)
{
	char* v;

	v = ui_widget_get_value("video.check.fullscreen");
	config_set_and_apply("wm.fullscreen",v);

	v = ui_widget_get_value("video.num.cap");
	config_set_and_apply("wm.framecap",v);

	v = ui_widget_get_value("video.num.width");
	config_set_and_apply("wm.width",v);

	v = ui_widget_get_value("video.num.height");
	config_set_and_apply("wm.height",v);

	v = ui_widget_get_value("video.check.ani");
	config_set_and_apply("gl.anisotropic",v);

	v = ui_widget_get_value("video.check.bil");
	config_set_and_apply("gl.bilinear",v);

	v = ui_widget_get_value("video.check.tri");
	config_set_and_apply("gl.trilinear",v);

	v = ui_widget_get_value("video.check.mip");
	config_set_and_apply("gl.mipmaps",v);

	return EVENT_HANDLED;
}

int start_singleplayer(widget_t *w)
{
	char* sd = ui_widget_get_value("singleopts.check.randomseed");
	if (sd)
		config_set("map.random_seed",sd);

	sd = ui_widget_get_value("singleopts.num.seed");
	if (sd)
		config_set("map.seed",sd);

	sd = ui_widget_get_value("singleopts.num.enemies");
	if (sd)
		config_set("game.ais",sd);

	sd = ui_widget_get_value("singleopts.num.difficulty");
	if (sd)
		config_set("game.difficulty",sd);

	sd = ui_widget_get_value("singleopts.num.turntime");
	if (sd)
		config_set("game.turn_time",sd);

	cmd_execf("exec ui.elements(*.!.*.style.visible) false");
	cmd_execf("exec ui.elements(game.!.*.style.visible) true");
	cmd_execf("exec ui.elements(console.!.*.style.visible) true");
	rtg_state = RTG_STATE_PLAY;

	client_connect_local();

	return EVENT_HANDLED;
}

int start_multiplayer(widget_t *w)
{
	return EVENT_HANDLED;
}

int gui_singleopts_show(widget_t *w)
{
	char* v = config_get("map.random_seed");
	ui_widget_set_value("singleopts.check.randomseed",v);

	v = config_get("map.seed");
	ui_widget_set_value("singleopts.num.seed",v);

	v = config_get("game.ais");
	ui_widget_set_value("singleopts.num.enemies",v);

	v = config_get("game.difficulty");
	ui_widget_set_value("singleopts.num.difficulty",v);

	v = config_get("game.turn_time");
	ui_widget_set_value("singleopts.num.turntime",v);

	return EVENT_HANDLED;
}

void gui_init()
{
	ui_widget_console_set("console.console.console");

	/* export functions used as ui callbacks */
	export_function("gui_sound_show",gui_sound_show);
	export_function("gui_sound_click",gui_sound_click);
	export_function("gui_music_show",gui_music_show);
	export_function("gui_music_click",gui_music_click);
	export_function("gui_effects_show",gui_effects_show);
	export_function("gui_effects_click",gui_effects_click);
	export_function("gui_controls_show",gui_controls_show);
	export_function("gui_controls_hide",gui_controls_hide);
	export_function("gui_video_show",gui_video_show);
	export_function("gui_video_save",gui_video_save);
	export_function("gui_singleopts_show",gui_singleopts_show);

	/* direct binds */
	ui_widget_event_set("game.button.map",map_toggle_ui,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
	ui_widget_event_set("map.button.close",map_toggle_ui,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
	ui_widget_event_set("singleopts.button.play",start_singleplayer,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
	ui_widget_event_set("town.button.vg-train-b",client_train_villager,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
	ui_widget_event_set("farm.button.fm-train-b",client_train_farmer,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
	ui_widget_event_set("farm.button.mf-train-b",client_train_master,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
	ui_widget_event_set("barracks.button.sd-train-b",client_train_soldier,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
	ui_widget_event_set("barracks.button.bg-train-b",client_train_brigadier,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
	ui_widget_event_set("barracks.button.st-train-b",client_train_scout,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
	ui_widget_event_set("attack.button.attack",client_attack,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
}
