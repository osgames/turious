CC ?= gcc
CD ?= cd
RM ?= rm
LD ?= ld
W32_CC ?= i686-pc-mingw32-gcc-4.6.3

TARGET=turious
PACKAGE=turious
VERSION=0.3
ARCH=POSIX

PWD=$(shell pwd)
PREFIX ?= /usr
BINDIR ?= $(PREFIX)/games/bin
DESTDIR ?= $(PREFIX)/games/$(PACKAGE)
DATADIR ?= $(PREFIX)/games
LIBDIR ?= $(PREFIX)/lib
INCDIR ?= $(PREFIX)/include/$(PACKAGE)

EXT ?=

DEP_LIBS=$(shell rtg-config --libs --static)
CFLAG_DEFS=-DPACKAGE=\"$(PACKAGE)\" -DVERSION=\"$(VERSION)\" -DGAMEDATA=\"$(DATADIR)\"

POSIX_CFLAGS ?= -Wall -g -Iinc/ -I. $(CFLAG_DEFS) $(CFLAGS) -pedantic $(shell rtg-config --cflags)
POSIX_CLIBS ?= $(DEP_LIBS) -g $(CLIBS)
POSIX_LDFLAGS ?= -g $(LDFLAGS)

W32_CFLAGS ?= -Wall -g -Iinc/ -I. -I/usr/i686-mingw32/usr/include/ -Iw32libs/ -Iw32libs/inc/ -mwindows -DWIN32
W32_CLIBS ?= -L$(PWD)/w32libs/lib/  -lpng -llua51 -lOpenAL32 -lfreetype -lz -lwinmm -lmingw32 -lopengl32 -mwindows
W32_LDFLAGS ?= -g

SRCDIR=src
DISTFILES=inc data README CHANGELOG LICENSE $(SRCDIR) Makefile
INSTFILES=data/default.cfg
INSTFONTS=data/fonts/font.ttf
INSTIMGS=data/images/ground.png data/images/orc.png data/images/select.bmp
INSTSNDS=data/sounds/explosion.ogg data/sounds/explosion.wav
INSTXMLS=data/xml/main.ui data/xml/animations.xml
INSTGRP ?= games

OBJS=$(SRCDIR)/ai.o $(SRCDIR)/client.o $(SRCDIR)/drawspec.o $(SRCDIR)/game.o $(SRCDIR)/gui.o $(SRCDIR)/lib.o $(SRCDIR)/main.o $(SRCDIR)/map.o $(SRCDIR)/mapgen.o $(SRCDIR)/noise.o $(SRCDIR)/path.o $(SRCDIR)/player.o $(SRCDIR)/scout.o $(SRCDIR)/shade.o $(SRCDIR)/texteffect.o $(SRCDIR)/unit.o $(SRCDIR)/village.o

ifeq "$(ARCH)" "POSIX"
	TARGET_CFLAGS=$(POSIX_CFLAGS)
	TARGET_CLIBS=$(POSIX_CLIBS)
	TARGET_LDFLAGS=$(POSIX_LDFLAGS)
else
	CC=$(W32_CC)
	EXT=.exe
	STATICOBJS=$(OBJS)
	TARGET_CFLAGS=$(W32_CFLAGS)
	DEP_LIBS=$(W32_CLIBS)
	TARGET_CLIBS=$(W32_CLIBS)
	TARGET_LDFLAGS=$(W32_LDFLAGS)
endif


LASTARCH=$(shell touch lastarch && cat lastarch)
ifeq "$(LASTARCH)" ""
	LASTARCH=$(ARCH)
endif

all: client

prepare:
ifneq "$(ARCH)" "$(LASTARCH)"
	@$(MAKE) clean
endif
	@echo $(ARCH) > lastarch

client: prepare $(OBJS)
	$(CC) -o $(TARGET)$(EXT) $(OBJS) $(TARGET_CLIBS)

w32:
	@$(MAKE) ARCH="W32"

w32-fresh: clean w32

local:
	make DESTDIR=$(PWD) DATADIR=$(PWD)/data CFLAGS="-DRTG_LOCAL=1" all

dist-base:
	mkdir -p $(PACKAGE)-$(VERSION)
	cp -Rt $(PACKAGE)-$(VERSION) $(DISTFILES)

dist-gz: dist-base
	tar czf $(PACKAGE)-$(VERSION).tar.gz $(PACKAGE)-$(VERSION)
	$(RM) -r $(PACKAGE)-$(VERSION)

dist-bz2: dist-base
	tar cjf $(PACKAGE)-$(VERSION).tar.bz2 $(PACKAGE)-$(VERSION)
	$(RM) -r $(PACKAGE)-$(VERSION)

dist: distclean dist-bz2

distclean:
	$(RM) lastarch
	$(RM) $(OBJS)

clean: distclean
	$(RM) $(PACKAGE)*

install: all
	install --group=$(INSTGRP) -d $(BINDIR)
	install --group=$(INSTGRP) $(PACKAGE) $(BINDIR)/$(PACKAGE)
	install --group=$(INSTGRP) -d $(DESTDIR)
	install --group=$(INSTGRP) -t $(DESTDIR) $(INSTFILES)
	install --group=$(INSTGRP) -d $(DESTDIR)/fonts
	install --group=$(INSTGRP) -t $(DESTDIR)/fonts $(INSTFONTS)
	install --group=$(INSTGRP) -d $(DESTDIR)/sounds
	install --group=$(INSTGRP) -t $(DESTDIR)/sounds $(INSTSNDS)
	install --group=$(INSTGRP) -d $(DESTDIR)/images
	install --group=$(INSTGRP) -t $(DESTDIR)/images $(INSTIMGS)
	install --group=$(INSTGRP) -d $(DESTDIR)/xml
	install --group=$(INSTGRP) -t $(DESTDIR)/xml $(INSTXMLS)

uninstall:

fresh: clean all

fresh-local: clean local

$(SRCDIR)/%.o: $(SRCDIR)/%.c inc/common.h
	$(CC) $(TARGET_CFLAGS) -o $@ -c $<


.PHONY: all default prepare test w32 w32-fresh local dist-base dist-gz dist-bz2 dist distclean clean install uninstall fresh fresh-local
